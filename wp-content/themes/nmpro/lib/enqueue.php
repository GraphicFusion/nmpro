<?php

namespace Sonder\Dorado\Assets;

/**
 * Scripts and stylesheets
 *
 * Enqueue stylesheets in the following order:
 * 1. /theme/assets/styles/main.css
 *
 * Enqueue scripts in the following order:
 * 1. /theme/assets/scripts/modernizr.js
 * 2. /theme/assets/scripts/main.js
 */

class JsonManifest {
  private $manifest;

  public function __construct($manifest_path) {
    if (file_exists($manifest_path)) {
      $this->manifest = json_decode(file_get_contents($manifest_path), true);
    } else {
      $this->manifest = array();
    }
  }

  public function get() {
    return $this->manifest;
  }

  public function getPath($key = '', $default = null) {
    $collection = $this->manifest;
    if (is_null($key)) {
      return $collection;
    }
    if (isset($collection[$key])) {
      return $collection[$key];
    }
    foreach (explode('.', $key) as $segment) {
      if (!isset($collection[$segment])) {
        return $default;
      } else {
        $collection = $collection[$segment];
      }
    }
    return $collection;
  }
}

function asset_path($filename) {
  $dist_path = get_template_directory_uri() . DIST_DIR;
  $directory = dirname($filename) . '/';
  $file = basename($filename);
  static $manifest;

  if (empty($manifest)) {
    $manifest_path = get_template_directory() . DIST_DIR . 'assets.json';
    $manifest = new JsonManifest($manifest_path);
  }
  if (array_key_exists($file, $manifest->get())) {
    return $dist_path . $directory . $manifest->get().array($file);
  } else {
    return $dist_path . $directory . $file;
  }
}

function assets() {
  wp_enqueue_style('dorado_css', asset_path('css/main.min.css'), false, null);
  wp_enqueue_style('colorbox', asset_path('css/vendor/colorbox.css'), false, null);
  wp_enqueue_style('slick', asset_path('css/vendor/slick.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
  wp_enqueue_script('countdown', asset_path('js/vendor/jquery.countdown.min.js'), array('jquery'), null, false);
  wp_enqueue_script('colorbox', asset_path('js/vendor/jquery.colorbox.js'), array('jquery'), null, true);
  wp_enqueue_script('modernizr', asset_path('js/vendor/modernizr-2.8.3.min.js'), array(), null, true);
  wp_enqueue_script('slick', asset_path('js/vendor/slick.min.js'), array('jquery'), null, true);
  wp_enqueue_script('dorado_js', asset_path('js/_main.js'), array('jquery'), null, true);

  wp_enqueue_script('map_js', get_template_directory_uri() . '/assets/js/vendor/map.js', array('jquery'), null, true);
//	if( get_page_template_slug($post->ID) == 'page-map.php' ) {
		wp_register_script( 'gmap', 'https://maps.googleapis.com/maps/api/js?v=3.exp', array(), '', true );	
		wp_enqueue_script( 'gmap');
//	}


}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);