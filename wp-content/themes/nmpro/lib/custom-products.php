<?php

/**
 * Product Custom Post Type
 */
add_action( 'init', 'register_product_post_type');
function register_product_post_type()
{ 
	register_post_type( 'product',
		array( 'labels' => 
			array(
				'name'               => 'Products',
				'singular_name'      => 'Product',
				'all_items'          => 'All Products',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Product',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Product',
				'new_item'           => 'New Product',
				'view_item'          => 'View Product',
				'search_items'       => 'Search Products',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Products post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-store',
			 'rewrite'	      => array( 'slug' => 'product', 'with_front' => false ),
			 'has_archive'      => 'products',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'author', 'thumbnail', 'revisions')
		)
	);
}

/**
 * Product Categories
 */
register_taxonomy( 'product_cat', 
	array('product'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Product Categories',
			'singular_name' 	=> 'Product Category',
			'search_items' 		=> 'Search Product Categories',
			'all_items' 		=> 'All Product Categories',
			'parent_item' 		=> 'Parent Product Category',
			'parent_item_colon' => 'Parent Product Category:',
			'edit_item' 		=> 'Edit Product Category',
			'update_item' 		=> 'Update Product Category',
			'add_new_item' 		=> 'Add New Product Category',
			'new_item_name' 	=> 'New Product Category Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'product-cat' ),
	)
);
function get_product_languages(){
	return array(	'en'=>'English','sp'=>'Espanol','fr'=>'Francais','it'=>'Italiano','de'=>'Deutsch');
}
$languages = get_product_languages();
foreach($languages as $prefix => $lang ){
	$langFields[] = array (
			'key' => 'field_5600574cb5080'.$prefix,
			'label' => $lang,
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		);
	$langFields[] = array (
		'key' => 'field_56005784b5082'.$prefix,
		'label' => 'Description ('.$lang.')',
		'name' => 'description_'.$prefix,
		'type' => 'wysiwyg',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'tabs' => 'all',
		'toolbar' => 'full',
		'media_upload' => 1,
	);
	$langFields[] = array (
		'key' => 'field_excerpt082'.$prefix,
		'label' => 'Excerpt ('.$lang.')',
		'name' => 'excerpt_'.$prefix,
		'type' => 'textarea',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'tabs' => 'all',
		'toolbar' => 'full',
		'media_upload' => 1,
	);
}
if( function_exists('acf_add_local_field_group') ){
	acf_add_local_field_group(array (
		'key' => 'group_56005741dce85',
		'title' => 'Products',
		'fields' => $langFields,
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));	
}