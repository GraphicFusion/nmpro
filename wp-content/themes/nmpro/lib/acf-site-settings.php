<?php
/* 
 * Options Page
 * Add ACF items to an options page 
 * */

if ( function_exists('acf_add_options_page') ) {

    acf_add_options_page(
        array(
            'page_title'    => 'Site Options',
            'menu_title'    => 'Site Options',
            'menu_slug'     => 'site-options',
            'capability'    => 'edit_posts',
            'parent_slug'   => '',
            'position'      => false,
            'icon_url'      => false
        )
    );
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Sitewide',
            'menu_title'    => 'Sitewide',
            'menu_slug'     => 'site-options-sitewide',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );    
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Header',
            'menu_title'    => 'Header',
            'menu_slug'     => 'site-options-header',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );    
    acf_add_options_sub_page(
        array(
            'page_title'    => 'Events Settings',
            'menu_title'    => 'Events Settings',
            'menu_slug'     => 'site-options-events',
            'capability'    => 'edit_posts',
            'parent_slug'   => 'site-options',
            'position'      => false,
            'icon_url'      => false
        )
    );    
}
if( function_exists('acf_add_local_field_group') ){
	$events_page_obj = get_page_by_title('events');
	$events_page = $events_page_obj->ID; 
	acf_add_local_field_group(array (
		'key' => 'group_56144f7d87b58',
		'title' => 'Featured Event',
		'fields' => array (
			array (
				'key' => 'field_56144fa5431bd',
				'label' => 'Featured Event',
				'name' => 'featured_event',
				'type' => 'post_object',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'event',
				),
				'taxonomy' => array (
				),
				'allow_null' => 0,
				'multiple' => 0,
				'return_format' => 'object',
				'ui' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => $events_page,
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	

	// Adds repeater rows to testimonials group
	acf_add_local_field_group(
		array (
			'key' => 'dorado_repeater_row',
			'title' => 'Row',
			'parent' => 'dorado_sidebar',
			'fields' => array (
				array (
					'key' => 'dorado_event_time',
					'label' => 'Times',
					'name' => 'dorado_event_time',
					'type' => 'text',
					'parent' => 'dorado_sidebar'
				),
				array (
					'key' => 'dorado-event-active',
					'label' => 'Active',
					'name' => 'dorado-event-active',
					'type' => 'true_false',
					'parent' => 'dorado_sidebar'
				),
			)
		)
	);
}
if( function_exists('acf_add_local_field_group') ){
	$youtube = "";
	if( isset( $_GET ) && array_key_exists( 'post', $_GET) ){
		$post = get_post( $_GET['post'] );
		// checks to see if is video admin page
		if( is_object( $post ) && 'video' == $post->post_type ){
			$youtube = get_field('video_youtube_link',$post->ID);
			$duration = get_field('video_duration',$post->ID);
			$vimeo = get_field('video_vimeo_link',$post->ID);
			if( $youtube || $vimeo ){
				if( $duration ){
					$min = floor( $duration/60 );
					$seconds = ( ($duration/60) - $min ) * 60;
					$instructions = "Duration: ". $min ." minutes and " .$seconds." seconds";
				}
				$mssg = "";
				if( $youtube ){
					$mssg = '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$youtube.'" frameborder="0" allowfullscreen></iframe>';					
				}
				else if( $vimeo ){
					$mssg = '<iframe src="'.$vimeo.'" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
				}
				$video_fields[] = array (
					'key' => 'field_55f20ee6bf155',
					'label' => 'Video Preview',
					'name' => '',
					'type' => 'message',
					'instructions' => $instructions,
					'required' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => $mssg,
					'esc_html' => 0,
				);
			}
		}
	}
//print_r($video_fields);
$video_fields[] = 			array (
				'key' => 'field_55f2007a572de',
				'label' => 'YouTube ID',
				'name' => 'video_youtube_link',
				'type' => 'text',
				'instructions' => 'Visit the Youtube video you want to include. <br>In the address bar you\'ll see: https://www.youtube.com/watch?v=vSK1DpjvXUo <br>Copy the id after ?v= and paste it here',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => 'Looks random like this: vSK1DpjvXUo',
			);
$video_fields[] = array (
				'key' => 'field_55f200ad572df',
				'label' => 'Vimeo Link',
				'name' => 'video_vimeo_link',
				'type' => 'url',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
			);

$video_fields[] = 			array (
				'key' => 'field_55f200c5572e0',
				'label' => 'Excerpt',
				'name' => 'video_excerpt',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => 'wpautop',
				'readonly' => 0,
				'disabled' => 0,
			);
$video_fields[] =	array (
				'key' => 'field_55f2010a572e1',
				'label' => 'Content',
				'name' => 'video_content',
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			);
	if( $youtube ){
		$youtube_choices = array (
			0=>'None',
			1=>'<img src="http://img.youtube.com/vi/'.$youtube.'/1.jpg">',
			2=>'<img src="http://img.youtube.com/vi/'.$youtube .'/2.jpg">',
			3=>'<img src="http://img.youtube.com/vi/'.$youtube .'/3.jpg">',
			4=>'<img src="http://img.youtube.com/vi/'.$youtube.'/0.jpg">',
			5=>'<img src="http://img.youtube.com/vi/'.$youtube.'/maxresdefault.jpg"> Max Res may not be available',
		);
	}
	else{
		$youtube_choices = array (
			0=>'None',
			1=>'n/a',
			2=>'n/a',
			3=>'n/a',
			4=>'n/a',
		);
	}
	$video_fields[] = 
		
			array (
				'key' => 'field_560e7b92288c3',
				'label' => 'YouTube Background Image',
				'name' => 'youtube_image',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => $youtube_choices,
				'default_value' => array (
				),
	);

$video_fields[] = array (
	'key' => 'field_55f20138572e2',
	'label' => 'Custom Background Image',
	'name' => 'video_background_image',
	'type' => 'image',
	'instructions' => 'Overrides the default (or selected) YouTube background.',
	'required' => 0,
	'conditional_logic' => 0,
	'wrapper' => array (
		'width' => '',
		'class' => '',
		'id' => '',
	),
	'return_format' => 'array'
);

$video_fields[] = array (
				'key' => 'field_duration572e2',
				'label' => 'Youtube Video Duration (seconds)',
				'name' => 'video_duration',
				'type' => 'text',
				'instructions' => 'Will automatically detect after saving if empty.',
				'required' => 0,
				'conditional_logic' => array (
/*					array (
						array (
							'field' => 'field_55edc06e752a3',
							'operator' => '==',
							'value' => '01',
						),
						array (
							'field' => 'field_55edc06e752a3',
							'operator' => '!=',
							'value' => '01',
						),
					),*/
				),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
			);
$video_fields[] = array (
	'key' => 'field_testimonialName',
	'label' => 'Name (for Testimonials only)',
	'name' => 'video_testimonial_name',
	'type' => 'text',
);

	$video_category =	array (
		'key' => 'field_55f241d94745f',
		'label' => 'Category',
		'name' => 'video_category',
		'type' => 'taxonomy',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '30',
			'class' => 'video-cat-select',
			'id' => '',
		),
		'taxonomy' => 'video_cat',
		'field_type' => 'select',
		'allow_null' => 0,
		'add_term' => 1,
		'save_terms' => 0,
		'load_terms' => 0,
		'return_format' => 'object',
		'multiple' => 0,
	);
	$video_select =	array (
		'key' => 'field_55f241fa47460',
		'label' => 'Video',
		'name' => 'video',
		'type' => 'select',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '70',
			'class' => 'videos-select',
			'id' => '',
		),
		'default_value' => array (
		),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'placeholder' => '',
		'disabled' => 0,
		'readonly' => 0,
	);
	$add_video = array (
			'key' => 'field_561555d0f3e8c',
			'label' => 'Add Video',
			'name' => 'add_video',
			'type' => 'true_false',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '10',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
		);

	$text_video_category =	array (
		'key' => 'field_text_video_cat',
		'label' => 'Category',
		'name' => 'video_category',
		'type' => 'taxonomy',
		'instructions' => '',
		'required' => 0,
		/*'conditional_logic' => array (
			array (
				array (
					'field' => 'field_561555d0f3e8c',
					'operator' => '==',
					'value' => '1',
				),
			),
		),	*/	
		'wrapper' => array (
			'width' => '30',
			'class' => 'video-cat-select',
			'id' => '',
		),
		'taxonomy' => 'video_cat',
		'field_type' => 'select',
		'allow_null' => 0,
		'add_term' => 1,
		'save_terms' => 0,
		'load_terms' => 0,
		'return_format' => 'object',
		'multiple' => 0,
	);
	$text_video_select =	array (
		'key' => 'field_text_video_select',
		'label' => 'Video',
		'name' => 'video',
		'type' => 'select',
		'instructions' => '',
		'required' => 0,
	/*	'conditional_logic' => array (
			array (
				array (
					'field' => 'field_561555d0f3e8c',
					'operator' => '==',
					'value' => '1',
				),
			),
		),	*/	
		'wrapper' => array (
			'width' => '60',
			'class' => 'videos-select',
			'id' => '',
		),
		'default_value' => array (
		),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'placeholder' => '',
		'disabled' => 0,
		'readonly' => 0,
	);

	$video_category_single =	array (
		'key' => 'field_55fvideosinglecat',
		'label' => 'Category',
		'name' => 'video_category',
		'type' => 'taxonomy',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '30',
			'class' => 'video-cat-select-single',
			'id' => '',
		),
		'taxonomy' => 'video_cat',
		'field_type' => 'select',
		'allow_null' => 0,
		'add_term' => 1,
		'save_terms' => 0,
		'load_terms' => 0,
		'return_format' => 'object',
		'multiple' => 0,
	);
	$video_select_single =	array (
		'key' => 'field_55videosingle',
		'label' => 'Video',
		'name' => 'video',
		'type' => 'select',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '70',
			'class' => 'videos-select',
			'id' => '',
		),
		'default_value' => array (
		),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'placeholder' => '',
		'disabled' => 0,
		'readonly' => 0,
	);

	acf_add_local_field_group(array (
		'key' => 'group_55f20065b42c1',
		'title' => 'Video',
		'fields' => $video_fields,
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'video',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

	global $post;
	$default_value = "";
	if( is_object($post)){
		$default_value = $post->post_title;
	}
	$event_video_fields[] = array (
		'key' => 'event_title',
		'label' => 'Event Title',
		'name' => 'event_title',
		'default_value' => $default_value,
		'type' => 'text' // or use text or textarea
	);
	$event_video_fields[] = array (
		'key' => 'event_sub_title',
		'label' => 'SubTitle',
		'name' => 'event_sub_title',
		'type' => 'text' // or use text or textarea
	);
	$event_video_fields[] = $video_category;
	$event_video_fields[] = $video_select;
	$event_video_fields[] = array (
		'key' => 'event_background_image',
		'label' => 'Background Image',
		'instructions' => 'Size should be x by x',
		'name' => 'event_background_image',
		'type' => 'image',
		'return_format' => 'array'
	);
	$event_video_fields[] = array (
		'key' => 'event_video_image',
		'label' => 'Video Image',
		'instructions' => 'Size should be x by x',
		'name' => 'event_video_image',
		'type' => 'image',
		'return_format' => 'array'
	);
	$event_video_fields[] = array (
		'key' => 'event_container_color',
		'label' => 'Color',
		'instructions' => 'Set color of background image overlay',
		'name' => 'event_container_color',
		'type' => 'color_picker' 
	);
	$event_video_fields[] = array (
		'key' => 'event_container_opacity',
		'label' => 'Opacity',
		'instructions' => 'Set opacity of background image overlay (from 0 to 100 , where 0 is transparent)',
		'name' => 'event_container_opacity',
		'type' => 'number' 
	);
	$events_video_layout = array (
		'key' => 'events_video_layoutb2d6',
		'name' => 'events_video_block',
		'label' => 'Event Video Section',
		'display' => 'block',
		'sub_fields' => $event_video_fields
	);

	$banner_image_fields[] = array (
		'key' => 'bi_background_image',
		'label' => 'Image',
		'instructions' => 'Size should be x by x',
		'name' => 'bi_background_image',
		'type' => 'image',
		'return_format' => 'array',
		'wrapper' => array (
			'width' => '30',
			'class' => '',
			'id' => '',
		),
	);
	$banner_image_fields[] = array (
		'key' => 'bi_title',
		'label' => 'Custom Title',
		'instructions' => 'Leave empty to use the Video title (if a video is added).',
		'name' => 'bi_title',
		'default_value' => $default_value,
		'type' => 'text' // or use text or textarea
	);
	$banner_image_fields[] = array (
		'key' => 'bi_subtitle',
		'label' => 'Subtitle (Optional)',
		'instructions' => 'Leave empty to show the Video\'s Category (if a video is added).',
		'name' => 'bi_subtitle',
		'type' => 'text', // or use text or textarea
	);
	$banner_image_fields[] = array (
		'key' => 'field_centeralign3e8c',
		'label' => 'Center Text',
		'name' => 'bi_center_block',
		'type' => 'true_false',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '10',
			'class' => '',
			'id' => '',
		),
		'message' => '',
		'default_value' => 0,
	);
	$banner_image_fields[] = array (
		'key' => 'bi_container_color',
		'label' => 'Color',
		'instructions' => 'Set color of background image overlay',
		'name' => 'bi_container_color',
		'type' => 'color_picker', 
		'wrapper' => array (
			'width' => '25',
			'class' => '',
			'id' => '',
		),
	);
	$banner_image_fields[] = array (
		'key' => 'bi_container_opacity',
		'label' => 'Opacity',
		'instructions' => 'Set opacity of background image overlay (from 0 to 100 , where 0 is transparent)',
		'name' => 'bi_container_opacity',
		'type' => 'number',
		'wrapper' => array (
			'width' => '25',
			'class' => '',
			'id' => '',
		),
	);
	$banner_image_layout = array (
		'key' => 'banner_image_layoutb2d6',
		'name' => 'banner_image_block',
		'label' => 'Full Screen Image',
		'display' => 'block',
		'sub_fields' => $banner_image_fields
	);

	$banner_video_fields[] = $text_video_category;
	$banner_video_fields[] = $text_video_select;
	$banner_video_fields[] = array (
		'key' => 'bv_title',
		'label' => 'Custom Title',
		'instructions' => 'Leave empty to use the Video title (if a video is added).',
		'name' => 'bv_title',
		'default_value' => $default_value,
		'type' => 'text' // or use text or textarea
	);
	$banner_video_fields[] = array (
		'key' => 'bv_subtitle',
		'label' => 'Subtitle (Optional)',
		'instructions' => 'Leave empty to show the Video\'s Category (if a video is added).',
		'name' => 'bv_subtitle',
		'type' => 'text', // or use text or textarea
	);
	$banner_video_fields[] = array (
		'key' => 'field_centeralign3e8c',
		'label' => 'Center Text',
		'name' => 'bv_center_block',
		'type' => 'true_false',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '10',
			'class' => '',
			'id' => '',
		),
		'message' => '',
		'default_value' => 0,
	);
	$banner_video_fields[] = array (
		'key' => 'bv_background_image',
		'label' => 'Background Image',
		'instructions' => 'Size should be x by x',
		'name' => 'bv_background_image',
		'type' => 'image',
		'return_format' => 'array',
		'wrapper' => array (
			'width' => '30',
			'class' => '',
			'id' => '',
		),
	);
	$banner_video_fields[] = array (
		'key' => 'bv_container_color',
		'label' => 'Color',
		'instructions' => 'Set color of background image overlay',
		'name' => 'bv_container_color',
		'type' => 'color_picker', 
		'wrapper' => array (
			'width' => '25',
			'class' => '',
			'id' => '',
		),
	);
	$banner_video_fields[] = array (
		'key' => 'bv_container_opacity',
		'label' => 'Opacity',
		'instructions' => 'Set opacity of background image overlay (from 0 to 100 , where 0 is transparent)',
		'name' => 'bv_container_opacity',
		'type' => 'number',
		'wrapper' => array (
			'width' => '25',
			'class' => '',
			'id' => '',
		),
	);
	$banner_video_layout = array (
		'key' => 'banner_video_layoutb2d6',
		'name' => 'banner_video_block',
		'label' => 'Full Screen Video',
		'display' => 'block',
		'sub_fields' => $banner_video_fields
	);

	$read_more_fields[] = array (
		'key' => 'read_more_main',
		'label' => 'Main Content',
		'name' => 'read_more_main',
		'type' => 'wysiwyg' // or use text or textarea
	);
	$read_more_fields[] = array (
		'key' => 'read_more_hidden',
		'label' => 'Hidden Content',
		'name' => 'read_more_hidden',
		'type' => 'wysiwyg' // or use text or textarea
	);
	$read_more_layout = array (
		'key' => 'read_more_layout',
		'name' => 'read_more_block',
		'label' => 'Read More Section',
		'display' => 'block',
		'sub_fields' => $read_more_fields
	);
	$key_speakers_fields[] = array (
			'key' => 'field_560ac3cfd50ba',
			'label' => 'Key Speakers',
			'name' => 'key_speakers',
			'type' => 'post_object',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array (
				0 => 'speaker',
			),
			'taxonomy' => array (
			),
			'allow_null' => 0,
			'multiple' => 1,
			'return_format' => 'object',
			'ui' => 1,
	);
$key_speakers_fields[] = array (
	'key' => 'field_keyspeakerbg',
		'label' => 'Background Image',
		'name' => 'key_speakers_image',
		'type' => 'image',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'return_format' => 'array'
	);
	$key_speakers_layout = array (
		'key' => 'key_speakers_layout',
		'name' => 'key_speakers_block',
		'label' => 'Key Speakers Section',
		'display' => 'block',
		'sub_fields' => $key_speakers_fields
	);
	$guest_speakers_fields[] = array (
			'key' => 'field_guest0ac3cfd50ba',
			'label' => 'Guest Speakers',
			'name' => 'guest_speakers',
			'type' => 'post_object',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array (
				0 => 'speaker',
			),
			'taxonomy' => array (
			),
			'allow_null' => 0,
			'multiple' => 1,
			'return_format' => 'object',
			'ui' => 1,
	);
	$guest_speakers_layout = array (
		'key' => 'guest_speakers_layout',
		'name' => 'guest_speakers_block',
		'label' => 'Guest Speakers Section',
		'display' => 'block',
		'sub_fields' => $guest_speakers_fields
	);

	$image_gallery_layout =	array (
					'key' => '5616e0e633408',
					'name' => 'image_gallery_block',
					'label' => 'Image Gallery Section',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_5616e0fd0bd88',
							'label' => 'Images',
							'name' => 'images',
							'type' => 'gallery',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'min' => '',
							'max' => '',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => '',
							'max_height' => '',
							'max_size' => '',
							'mime_types' => '',
						),
					),
				);


$learn_more_layout =  
				array (
					'key' => '5615f1350f3af',
					'name' => 'learn_more_block',
					'label' => 'Learn More (Q&A)',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_5615f16e6004a',
							'label' => 'Learn More',
							'name' => 'learn_more_repeater',
							'type' => 'repeater',
							'instructions' => '',
							'layout' => 'table',
							'button_label' => 'Add a Q&A',
							'sub_fields' => array (
								array (
									'key' => 'field_5615f1d7e8c35',
									'label' => 'Question',
									'name' => 'lm_question',
									'type' => 'text',
								),
								array (
									'key' => 'field_5615f1e0e8c36',
									'label' => 'Answer',
									'name' => 'lm_answer',
									'type' => 'text',
								),
							),
						),
					),
			);

	$video_testimonial_fields[] = array (
		'key' => 'field_560aff5f3000a',
		'label' => 'Testimonials',
		'name' => 'video_testimonial_repeater',
		'type' => 'post_object',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '50',
			'class' => '',
			'id' => '',
		),
		'post_type' => array (
			0 => 'video',
		),
		'taxonomy' => array (
			0 => 'video_cat:testimonial',
		),
		'allow_null' => 0,
		'multiple' => 1,
		'return_format' => 'object',
		'ui' => 1,
	);
	$video_testimonial_layout = array (
		'key' => 'video_testimonial_layout',
		'name' => 'video_testimonial_block',
		'label' => 'Video Testimonials Section',
		'display' => 'block',
		'sub_fields' => $video_testimonial_fields
	);
	$events_landing_layouts =	array (
		'key' => 'field_event_landing_content',
		'label' => 'Event Landing Content',
		'name' => 'event_landing_content',
		'type' => 'flexible_content',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'button_label' => 'Add a Content Section',
		'min' => '',
		'max' => '',
		'layouts' => array (
			$events_video_layout,
			$read_more_layout,
			$key_speakers_layout,
			$guest_speakers_layout,
			$video_testimonial_layout,
			$learn_more_layout
		),
	);

	$events_layout = array (
		'key' => '55faddd18b2d6',
		'name' => 'events_block',
		'label' => 'Events Section',
		'display' => 'block',
		'sub_fields' => array (
			array (
				'key' => 'field_numofeventsd8b2d9',
				'label' => 'Number of Events',
				'name' => 'number_of_events',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55fadfad8b2d8',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => '75',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 1,
				'max' => '',
				'step' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_55fae06a8b2da',
				'label' => 'Choose Events to Include',
				'name' => 'events_repeater',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55fadfad8b2d8',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => '75',
					'class' => '',
					'id' => '',
				),
				'min' => '',
				'max' => '',
				'layout' => 'table',
				'button_label' => 'Add Event',
				'sub_fields' => array (
					array (
						'key' => 'field_55fae0908b2db',
						'label' => 'Event',
						'name' => 'event',
						'type' => 'post_object',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => array (
							0 => 'event',
						),
						'taxonomy' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'return_format' => 'object',
						'ui' => 1,
					),
				),
			),
			array (
				'key' => 'field_55fadfad8b2d8',
				'label' => 'Or Automatically Grab Events',
				'name' => 'automatic_events',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '25',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
			),
		),
		'min' => '',
		'max' => '',
	);
	$video_layout = array (
					'key' => '5videodd18b2d6',
					'name' => 'videos_block',
					'label' => 'Videos Gallery Section',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_videos88b2d7',
							'label' => 'Video Section Title',
							'name' => 'video_block_title',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
						array (
							'key' => 'field_numofvideos8b2d9',
							'label' => 'Number of Videos',
							'name' => 'number_of_videos',
							'type' => 'number',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_55favideograb',
										'operator' => '==',
										'value' => '1',
									),
								),
							),
							'wrapper' => array (
								'width' => '75',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => 1,
							'max' => '4',
							'step' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
						array (
							'key' => 'field_55favideoa8b2da',
							'label' => 'Choose Videos to Include',
							'name' => 'videos_repeater',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_55favideograb',
										'operator' => '!=',
										'value' => '1',
									),
								),
							),
							'wrapper' => array (
								'width' => '75',
								'class' => '',
								'id' => '',
							),
							'min' => '',
							'max' => '4',
							'layout' => 'table',
							'button_label' => 'Add Video',
							'sub_fields' => array (
								$video_category, $video_select
							),
							'return_format' => 'object',

						),
						array (
							'key' => 'field_55favideograb',
							'label' => 'Or Automatically Grab Videos',
							'name' => 'automatic_videos',
							'type' => 'true_false',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '25',
								'class' => '',
								'id' => '',
							),
							'message' => '',
							'default_value' => 0,
						),
					),
					'min' => '',
					'max' => '',
				);	
	$video_block_layout = array (
		'key' => 'videosectionlayout',
		'name' => 'video_block',
		'label' => 'Video Section',
		'display' => 'block',
		'sub_fields' => array (
			$video_category_single, $video_select_single
		),
		'min' => '',
		'max' => '',
	);
$floating_fields[] = array (
			'key' => 'field_560e95e424362',
			'label' => 'Background Image',
			'name' => 'floating_background_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		);
$floating_fields[] = array (
			'key' => 'field_560e95f524363',
			'label' => 'Title',
			'name' => 'floating_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		);
$floating_fields[] = array (
			'key' => 'field_floatingcontent',
			'label' => 'Content',
			'name' => 'floating_content',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		);
$floating_fields[] = array (
			'key' => 'field_radio560e960824364',
			'label' => 'Alignment',
			'name' => 'floating_align',
			'type' => 'radio',
			'instructions' => 'Defaults to text block floating left.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'left' => 'Left',
				'center' => 'Center (one column)',
				'right' => 'Right'
			),
			'other_choice' => 0,
			'save_other_choice' => 0,
			'default_value' => '',
			'layout' => 'vertical',
		);
$floating_fields[] = array (
			'key' => 'field_560e962224365',
			'label' => 'Button Link',
			'name' => 'floating_link',
			'type' => 'page_link',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		);
$floating_fields[] = array (
			'key' => 'field_560e963524366',
			'label' => 'Button Text',
			'name' => 'floating_button_text',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		);
		$floating_block_layout = array (
			'key' => 'field_floatingsectionlayout',
			'name' => 'floating_block',
			'label' => 'Floating Text Section',
			'display' => 'block',
			'sub_fields' => 
				$floating_fields
			,
			'min' => '',
			'max' => '',
		);


	$buy_now_fields[] = array (
		'key' => 'field_buy_now_image',
		'label' => 'Background Image',
		'name' => 'buy_now_background_image',
		'type' => 'image',
		'return_format' => 'array',
		'preview_size' => 'thumbnail',
		'library' => 'all',
	);
	$buy_now_fields[] = array (
		'key' => 'field_buy_nowcontent',
		'label' => 'Content',
		'name' => 'buy_now_content',
		'type' => 'textarea',
		'instructions' => 'For Products Pages only. Buy Now button will link to infusionsoft link.'
	);
	$buy_now_fields[] = array (
		'key' => 'field_buy_now_right_align',
		'label' => 'Right Align',
		'name' => 'buy_now_right_align',
		'type' => 'true_false',
		'instructions' => 'Defaults to left align.',
		'layout' => 'vertical',
	);
	$buy_now_block_layout = array (
		'key' => 'field_buy_nowsectionlayout',
		'name' => 'buy_now_block',
		'label' => 'Buy Now Section',
		'display' => 'block',
		'sub_fields' => 
			$buy_now_fields
		,
		'min' => '',
		'max' => '',
	);



	$product_gallery_layout = array (
					'key' => '5productdd18b2d6',
					'name' => 'products_block',
					'label' => 'Products Gallery Section',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_numofproducts8b2d9',
							'label' => 'Number of Products',
							'name' => 'number_of_products',
							'type' => 'number',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_55faproductgrab',
										'operator' => '==',
										'value' => '1',
									),
								),
							),
							'wrapper' => array (
								'width' => '75',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => 1,
							'max' => '4',
							'step' => '',
							'readonly' => 0,
							'disabled' => 0,
						),
						array (
							'key' => 'field_55faproducta8b2da',
							'label' => 'Choose Products to Include',
							'name' => 'products_repeater',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_55faproductgrab',
										'operator' => '!=',
										'value' => '1',
									),
								),
							),
							'wrapper' => array (
								'width' => '75',
								'class' => '',
								'id' => '',
							),
							'min' => '',
							'max' => '',
							'layout' => 'table',
							'button_label' => 'Add product',
							'sub_fields' => array (
								array (
									'key' => 'field_product',
									'label' => 'Product',
									'name' => 'product',
									'type' => 'post_object',
									'instructions' => '',
									'required' => 0,
									'conditional_logic' => 0,
									'wrapper' => array (
										'width' => '',
										'class' => '',
										'id' => '',
									),
									'post_type' => array (
										0 => 'product',
									),
									'taxonomy' => array (
									),
									'allow_null' => 0,
									'multiple' => 0,
									'return_format' => 'object',
									'ui' => 1,
								),
							),
						),
						array (
							'key' => 'field_55faproductgrab',
							'label' => 'Or Automatically Grab Products',
							'name' => 'automatic_products',
							'type' => 'true_false',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '25',
								'class' => '',
								'id' => '',
							),
							'message' => '',
							'default_value' => 0,
						),
					),
					'min' => '',
					'max' => '',
				);
	$testimonial_gallery_layout = array (
					'key' => '5testimonialdd18b2d6',
					'name' => 'testimonials_block',
					'label' => 'Testimonials Slideshow Section',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_numoftestimonials8b2d9',
							'label' => 'Number of testimonials',
							'name' => 'number_of_testimonials',
							'type' => 'number',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_55fatestimonialgrab',
										'operator' => '==',
										'value' => '1',
									),
								),
							),
							'wrapper' => array (
								'width' => '75',
								'class' => '',
								'id' => '',
							),
							'min' => 1,
							'max' => '4',
						),
						array (
							'key' => 'field_55fatestimoniala8b2da',
							'label' => 'Choose testimonials to Include',
							'name' => 'testimonials_repeater',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_55fatestimonialgrab',
										'operator' => '!=',
										'value' => '1',
									),
								),
							),
							'wrapper' => array (
								'width' => '75',
								'class' => '',
								'id' => '',
							),
							'layout' => 'table',
							'button_label' => 'Add testimonial',
							'sub_fields' => array (
								array (
									'key' => 'field_testimonial',
									'label' => 'Testimonial Name',
									'name' => 'testimonial',
									'type' => 'post_object',
									'instructions' => '',
									'post_type' => array (
										0 => 'testimonial',
									),
									'return_format' => 'object',
									'ui' => 1,
								),
							),
						),
						array (
							'key' => 'field_55fatestimonialgrab',
							'label' => 'Or Automatically Grab testimonials',
							'name' => 'automatic_testimonials',
							'type' => 'true_false',
							'instructions' => '',
							'wrapper' => array (
								'width' => '25',
							),
							'message' => '',
							'default_value' => 0,
						),
					),
					'min' => '',
					'max' => '',
				);
				$layouts = array (
					$events_layout,
					$video_layout,
					$product_gallery_layout,
					$testimonial_gallery_layout,
					$banner_video_layout,
					$banner_image_layout,
					$floating_block_layout,
					$learn_more_layout,
					$image_gallery_layout,
					$read_more_layout,
					$buy_now_block_layout
				);
				$home = get_page_by_title('home');
				if( ( is_admin() && is_object($home) && array_key_exists('post', $_GET) && ( $home->ID != $_GET['post'] ) ) || !is_admin() ){
					$layouts[] = $video_testimonial_layout;
				}
	$custom_layouts =	array (
		'key' => 'field_55f5d42e82ba8',
		'label' => 'Custom Sections',
		'name' => 'custom_content',
		'type' => 'flexible_content',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'button_label' => 'Add a Content Section',
		'min' => '',
		'max' => '',
		'layouts' => $layouts,
	);

acf_add_local_field_group(array (
	'key' => 'group_5617120262d55',
	'title' => 'Product Content',
	'fields' => array (
		array (
			'key' => 'field_toggle_product_title',
			'label' => 'Toggle Product Title',
			'name' => 'toggle_product_title',
			'type' => 'true_false',
			'wrapper' => array (
				'width' => '50',
				'class' => '',
				'id' => '',
			)
		),
		array (
			'key' => 'field_toggle_language',
			'label' => 'Ask Customer Preferred Language',
			'name' => 'toggle_language',
			'type' => 'true_false',
			'wrapper' => array (
				'width' => '50',
				'class' => '',
				'id' => '',
			)
		),
		array (
			'key' => 'field_product_sidebar_button',
			'label' => 'Sidebar Button Text',
			'name' => 'sidebar_button_text',
			'type' => 'text',
		),
		array (
			'key' => 'field_5617122859b71',
			'label' => 'Sidebar Content',
			'name' => 'sidebar_content',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
		),
		array (
			'key' => 'field_infusionsoft9b71',
			'label' => 'Infusionsoft Link',
			'name' => 'infusionsoft_link',
			'type' => 'text',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'product',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


	$product_layouts =	array (
		'key' => 'field_ProductLayouts',
		'label' => 'Product Header',
		'name' => 'product_header',
		'type' => 'flexible_content',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'button_label' => 'Add the Product Header',
		'min' => '',
		'max' => '',
		'layouts' => array (
			$banner_video_layout,
			$banner_image_layout,
		),
	);
	$custom_product_content = array( $product_layouts ) ; 
	acf_add_local_field_group(array (
		'key' => 'group_55fcustomproductcontent',
		'title' => 'Product Header',
		'fields' => $custom_product_content,
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	$custom_content = array( $custom_layouts) ; 
	acf_add_local_field_group(array (
		'key' => 'group_55fcustomcontent',
		'title' => 'Custom Contents',
		'fields' => $custom_content,
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'template-custom.php',
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'product',
				),
			),
		),
		'menu_order' => 1,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	$videos_page = get_page_by_title('videos');
	acf_add_local_field_group(array (
		'key' => 'group_5616aeb8c6709',
		'title' => 'Featured',
		'fields' => array (
			array (
				'key' => 'field_5616aec063799',
				'label' => 'Add a Featured Video',
				'name' => 'video_archive_custom_content',
				'type' => 'flexible_content',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'button_label' => 'Add a Featured Video',
				'min' => '',
				'max' => '',
				'layouts' => array (
					$video_block_layout
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => $videos_page->ID,
				),
			),
		),
	));



}
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5602c66657038',
	'title' => 'Header',
	'fields' => array (
		array (
			'key' => 'field_5602c66ec1857',
			'label' => 'Call Link Text',
			'name' => 'call_link_text',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5602c6a6c1858',
			'label' => 'Call Link Number',
			'name' => 'call_link_number',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'site-options-header',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
function acf_load_sidebar_choices( $field ) {
    
	$nameField = get_field_object('sb_block_name');
	$key = $nameField['key']; 

    // reset choices
    $field['choices'] = array( 'events'=>'Events' , 'donate'=>'Donate');
   
    // get the sidebar block name from options page without any formatting
    $choices = get_field('sb_flexible_content', 'option', false);
	foreach( $choices as $i => $array){
		$name = $array[$key];
		$field['choices'][ $name ] = $name;
	}      
    return $field;
}
add_filter('acf/load_field/name=sb_builder_block', 'acf_load_sidebar_choices');

function get_acf_sidebar( $post_id ){
	if( get_field('add_sidebar' , $post_id ) ){
		// check if default
		$use_custom_sidebar = get_field('use_custom_sidebar' , $post_id );
		if( $use_custom_sidebar ){
			$type = $post_id;
		}
		else{	
			$type = 'option';
		}
		if( have_rows('sidebar_builder_blocks', $type) ):
		    while ( have_rows('sidebar_builder_blocks', $type) ) : the_row();
	
		        $block_names[] = get_sub_field('sb_builder_block');
		
		    endwhile;
		
		else :
		
		    // no rows found
		
		endif;
		foreach($block_names as $key => $block_name){
			if( $block_name == 'events'){
				$blocks[] = 'events';
			}
			elseif( $block_name == 'donate' ){
				$blocks[] = 'donate';
			}
			else{
				$blocks[] = get_sidebar_block( $block_name );	
			}
		}	
		return $blocks;
	}
	else{
		return false;
	}
}
function get_sidebar_block( $name ){
    $defined_blocks = get_field('sb_flexible_content', 'option', false);
	$nameField = get_field_object('sb_block_name');
	$key = $nameField['key']; 
	foreach( $defined_blocks as $i => $block ){
		if( $name == $block[$key] ){
			$returnBlock = $block;
			break;
		}
	}
	return $returnBlock;
}
function get_event_block(){
	$block = array();

	return $block;
}
function build_sidebar( $sidebar ){
	$nameField = get_field_object('sb_block_name');
	$nameKey = $nameField['key']; 

	$titleField = get_field_object('sb_block_title');
	$title = $titleField['key']; 

	$textField = get_field_object('sb_text_repeater');
	$textRows = $textField['key']; 

	$textField = get_field_object('sb_text_content');
	$text = $textField['key']; 

	$textFontField = get_field_object('sb_text_font');
	$textFont = $textFontField['key']; 

	$textColorField = get_field_object('sb_text_color');
	$textColor = $textColorField['key']; 

	$infoField = get_field_object('sb_info_repeater');
	$info = $infoField['key']; 

	$infoLabel = get_field_object('sb_info_label');
	$label = $infoLabel['key']; 

	$labelFontField = get_field_object('sb_label_font');
	$labelFont = $labelFontField['key']; 

	$labelColorField = get_field_object('sb_label_color');
	$labelColor = $labelColorField['key']; 

	$infoValue = get_field_object('sb_info_value');
	$value = $infoValue['key']; 

	$valueFontField = get_field_object('sb_value_font');
	$valueFont = $valueFontField['key']; 

	$valueColorField = get_field_object('sb_value_color');
	$valueColor = $valueColorField['key']; 

	foreach( $sidebar as $i => $block ): 
		if( $block == 'events' ): ?>

			<div class='sidebar-block events'>
				<div class='sidebar-title'>Events</div>
				  	  <?php get_template_part('templates/content', 'events'); ?>
				<div class='sidebar-text'></div>
			</div>
		
		<?php elseif( $block == 'donate') : ?>

			<div class='sidebar-block donate'>
				<div class='sidebar-title'>Donate/Support</div>
				<div class='sidebar-text'>
					<label>Choose Your Amount</label>	
					<?php
						$donate_page_id = get_page_by_title('donate'); 
						$buttons = get_field('buttons' , $donate_page_id );
						if($buttons) { 
							$out = "<select id='donate_option'>";
							foreach( $buttons as $button ){
								$out .= "<option value='". $button['donation_link'] ."'>". $button['donation_amount'] . " </option>";
							}
							$out .= "</select>";
							echo $out;
						}
					?>
					<div class='sidebar-title' id='submit_donate'>Donate</div>
				</div>
			</div>

		<?php else : ?>

		<div class='sidebar-block'>
			<div class='sidebar-title'><?php echo $block[$title]; ?></div>
			<div class='sidebar-text'>

			<?php if( is_array ( $block[$info] ) ): ?>
				<?php foreach( $block[$info] as $row ): ?>

					<div class='info-row row'>
						<div class='col-xs-12 col-sm-6 col-lg-3'>
							<label class='<?php echo $row[$labelFont]. " " .$row[$labelColor]; ?>'><?php echo $row[$label]; ?></label>
						</div>
						<div class='col-xs-12 col-sm-6 col-lg-9'>
							<div class='info-text <?php echo $row[$valueFont]. " " .$row[$valueColor]; ?>'><?php echo $row[$value]; ?></div>
						</div>
					</div>
				
				<?php endforeach; ?>
			<?php endif; ?>

			<?php if( is_array ( $block[$textRows] ) ): ?>
				<?php foreach( $block[$textRows] as $row ): ?>

					<div class='text-row'>
						<div class='text-content <?php echo $row[$textFont]. " " .$row[$textColor]; ?>'><?php echo $row[$text]; ?></div>
					</div>
				
				<?php endforeach; ?>
			<?php endif; ?>

			</div>
		</div>	
		
	<?php endif; endforeach; 
}
?>