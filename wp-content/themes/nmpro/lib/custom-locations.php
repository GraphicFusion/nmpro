<?php

/**
 * Location Custom Post Type
 */
add_action( 'init', 'register_location_post_type');
function register_location_post_type()
{ 
	register_post_type( 'location',
		array( 'labels' => 
			array(
				'name'               => 'Locations',
				'singular_name'      => 'Location',
				'all_items'          => 'All Locations',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Location',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Location',
				'new_item'           => 'New Location',
				'view_item'          => 'View Location',
				'search_items'       => 'Search Locations',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Locations post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-location-alt',
			 'rewrite'	      => array( 'slug' => 'Location', 'with_front' => false ),
			 'has_archive'      => 'Locations',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'author', 'thumbnail', 'revisions')
		)
	);
}

/**
 * Location Categories
 */
register_taxonomy( 'location_cat', 
	array('location'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Location Categories',
			'singular_name' 	=> 'Location Category',
			'search_items' 		=> 'Search Location Categories',
			'all_items' 		=> 'All Location Categories',
			'parent_item' 		=> 'Parent Location Category',
			'parent_item_colon' => 'Parent Location Category:',
			'edit_item' 		=> 'Edit Location Category',
			'update_item' 		=> 'Update Location Category',
			'add_new_item' 		=> 'Add New Location Category',
			'new_item_name' 	=> 'New Location Category Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'location-cat' ),
	)
);