<?php

add_action( 'admin_init', 'egg_dependencies' );

/**
 * Check for dependencies
 */
function egg_dependencies()
{
	if ( ! class_exists( 'acf' ) )
	{
		add_action( 'admin_notices', 'egg_acf_dependency_message' );
	}
}


/**
 * Add a nag for required dependencies that are missing
 */
function egg_acf_dependency_message() { ?>
	<div class="update-nag">
		This theme requires the <a href="http://wordpress.org/plugins/advanced-custom-fields/">Advanced Custom Fields</a> plugin to be installed and activated.
	</div>
<?php } 

	/**
	 * Setup Plugin List to only show for Admin
	 */
	function hidePlugins() {
		global $wp_list_table;
		$hidearr = array(
			'wp-migrate-db-pro/wp-migrate-db-pro.php',
			'advanced-custom-fields-pro/acf.php'
		);
		$myplugins = $wp_list_table->items;
		foreach ($myplugins as $key => $val) {
			if (in_array($key,$hidearr)) {
				unset($wp_list_table->items[$key]);
			}
		}
	}
	function hideSEOPlugins() {
		global $wp_list_table;
		$hidearr = array(
			'wordpress-seo/wp-seo.php'
		);
		$myplugins = $wp_list_table->items;
		foreach ($myplugins as $key => $val) {
			if (in_array($key,$hidearr)) {
				unset($wp_list_table->items[$key]);
			}
		}
	}
	$admin = false;
	$is_seo = false;
	if ( is_user_logged_in() ) {
		global $user_id;
		$user = new WP_User( $user_ID );
		if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
			foreach ( $user->roles as $role )
				if( $role == 'gfd_admin' ){
					$admin = true;
				}
				if( $role == 'gfd_seo' ){
					$is_seo = true;
				}
		}
	}
	if( !$admin) {
		add_action( 'pre_current_active_plugins', 'hidePlugins' );
	}
	if( !$is_seo ){
		add_action( 'pre_current_active_plugins', 'hideSEOPlugins' );
	}

add_action( 'admin_init', 'hide_editor' );

function hide_editor() {
	// Get the Post ID.
	
	$post_id = @$_GET['post'] ? $_GET['post'] : @$_POST['post_ID'] ;
	if( !isset( $post_id ) ) return;

	// Get the name of the Page Template file.
	$template_file = get_post_meta($post_id, '_wp_page_template', true);
    
    if($template_file == 'template-custom.php'){ // edit the template name
    	remove_post_type_support('page', 'editor');
    }
}
add_action('admin_menu', 'custom_admin_menus');
function custom_admin_menus() {
	add_submenu_page( 'edit.php?post_type=event', 'Add Live Event', 'Add Live Event', 'manage_options', '/post-new.php?post_type=event&is_live=true' );
	add_submenu_page( 'edit.php?post_type=event', 'Speakers', 'Speakers', 'manage_options', 'edit.php?post_type=speaker' );
	add_submenu_page( 'edit.php?post_type=event', 'Locations', 'Locations', 'manage_options', 'edit.php?post_type=location' );
	add_submenu_page( 'edit.php?post_type=product', 'Testimonials', 'Testimonials', 'manage_options', 'edit.php?post_type=testimonial' );
	remove_submenu_page( 'edit.php?post_type=event', 'edit-tags.php?taxonomy=event_cat&amp;post_type=event' );
	remove_submenu_page( 'edit.php?post_type=event', 'post-new.php?post_type=event' );
	remove_menu_page( 'edit.php?post_type=speaker');
	remove_menu_page( 'edit.php?post_type=location');
	remove_meta_box( 'authordiv', 'event', 'normal' );
	remove_meta_box( 'authordiv', 'speaker', 'sidebar' );
	remove_meta_box( 'authordiv', 'video', 'sidebar' );
	remove_meta_box('revisionsdiv', 'page', 'normal');
	remove_meta_box('revisionsdiv', 'event', 'normal');
	remove_meta_box('revisionsdiv', 'speaker', 'normal');
	remove_meta_box('revisionsdiv', 'video', 'normal');
	add_menu_page('View More Options', 'More Options', 'manage_options', '/view_more', '','dashicons-arrow-down-alt2');
}

function custom_menu_order($m) {
    if (!$m) return true;
	if( $m[0] == 'wpengine-common' ){
		array_shift($m);
	}
	$n = array();
	foreach( $m as $key=>$name ){
		switch ($name) {
		    case 'index.php':
		        $n[0] = $m[$key];
		        break;
		    case 'edit.php':
		        $n[8] = $m[$key];
		        break;
		    case 'upload.php':
		        $n[9] = $m[$key];
		        break;
		    case 'edit.php?post_type=product':
		        $n[1] = $m[$key];
		        break;
		    case 'edit.php?post_type=event':
		        $n[2] = $m[$key];
		        break;
		    case 'edit.php?post_type=video':
		        $n[4] = $m[$key];
		        break;
		    case 'gf_edit_forms':
		        $n[10] = $m[$key];
		        break;
		    case 'edit.php?post_type=page':
		        $n[9] = $m[$key];
		        break;
		    case 'edit-comments.php':
		        $n[11] = $m[$key];
		        break;
		    case 'themes.php':
		        $n[12] = $m[$key];
		        break;
		    case 'plugins.php':
		        $n[13] = $m[$key];
		        break;
		    case 'users.php':
		        $n[14] = $m[$key];
		        break;
		    case 'tools.php':
		        $n[15] = $m[$key];
		        break;
		    case 'options-general.php':
		        $n[16] = $m[$key];
		        break;
		    case 'edit.php?post_type=acf-field-group':
		        $n[7] = $m[$key];
		        break;
		    case 'site-options-sitewide':
		        $n[5] = $m[$key];
		        break;
		    case 'view_more':
		        $n[6] = $m[$key];
		        break;
		}
	}
/*	$n[0] = $m[0];
	$n[1] = $m[4];
	$n[2] = $m[5];
	$n[3] = $m[9];
	$n[4] = $m[6];
	$n[5] = $m[18];
	$n[6] = $m[20];
	$n[7] = $m[17];
	$n[8] = $m[2];
	$n[9] = $m[3];
	$n[10] = $m[7];
	$n[] = $m[10];
	$n[] = $m[12];
	$n[] = $m[13];
	$n[] = $m[14];
	$n[] = $m[15];
	$n[] = $m[16];*/
	return $n;
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');

add_action('init', 'cloneAdmin');
function cloneAdmin(){
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('administrator');
    //Adding a 'new_role' with all admin caps
    $wp_roles->add_role('gfd_admin', 'Graphic Fusion SuperAdmin', $adm->capabilities);
    $wp_roles->add_role('gfd_seo', 'Graphic Fusion SEO', $adm->capabilities);
}

function dorado_role_admin_body_class( $classes ) {
    global $current_user;
    foreach( $current_user->roles as $role )
        $classes .= ' role-' . $role;
    return trim( $classes );
}
add_filter( 'admin_body_class', 'dorado_role_admin_body_class' );

add_action( 'admin_init','nmp_custom_menu_class' );
function nmp_custom_menu_class() 
{
    global $menu;
    foreach( $menu as $key => $value )
    {
		if( $menu[$key][2] != 'view_more' && $key > 6 ){
//            $menu[$key][4] .= " nmp-hide-menu-item";
		}
    }
}