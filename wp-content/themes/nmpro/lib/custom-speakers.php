<?php

/**
 * Speaker Custom Post Type
 */
add_action( 'init', 'register_speaker_post_type');
function register_speaker_post_type()
{ 
	register_post_type( 'speaker',
		array( 'labels' => 
			array(
				'name'               => 'Speakers',
				'singular_name'      => 'Speaker',
				'all_items'          => 'All Speakers',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Speaker',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Speaker',
				'new_item'           => 'New Speaker',
				'view_item'          => 'View Speaker',
				'search_items'       => 'Search Speakers',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Speakers post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-megaphone',
			 'rewrite'	      => array( 'slug' => 'Speaker', 'with_front' => false ),
			 'has_archive'      => 'Speakers',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'author', 'thumbnail', 'revisions')
		)
	);
}

/**
 * Speaker Categories
 */
register_taxonomy( 'speaker_cat', 
	array('speaker'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Speaker Categories',
			'singular_name' 	=> 'Speaker Category',
			'search_items' 		=> 'Search Speaker Categories',
			'all_items' 		=> 'All Speaker Categories',
			'parent_item' 		=> 'Parent Speaker Category',
			'parent_item_colon' => 'Parent Speaker Category:',
			'edit_item' 		=> 'Edit Speaker Category',
			'update_item' 		=> 'Update Speaker Category',
			'add_new_item' 		=> 'Add New Speaker Category',
			'new_item_name' 	=> 'New Speaker Category Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'speaker-cat' ),
	)
);
// Changes text on title input box in admin
add_filter('gettext','custom_speaker_title');
function custom_speaker_title( $input ) {
    global $post_type;
    if( is_admin() && 'Enter title here' == $input && 'speaker' == $post_type )
        return 'Speaker Name';
    return $input;
}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_560ac178b53c0',
	'title' => 'Speaker Information',
	'fields' => array (
		array (
			'key' => 'field_560ac1ee3b220',
			'label' => 'Speaker Image',
			'name' => 'speaker_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_560ac1836b8ea',
			'label' => 'Tagline',
			'name' => 'speaker_tagline',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_560ac2113b221',
			'label' => 'Bio',
			'name' => 'speaker_bio',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'speaker',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;