<?php

/**
 * Testimonial Custom Post Type
 */
add_action( 'init', 'register_testimonial_post_type');
function register_testimonial_post_type()
{ 
	register_post_type( 'testimonial',
		array( 'labels' => 
			array(
				'name'               => 'Testimonials',
				'singular_name'      => 'Testimonial',
				'all_items'          => 'All Testimonials',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Testimonial',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Testimonial',
				'new_item'           => 'New Testimonial',
				'view_item'          => 'View Testimonial',
				'search_items'       => 'Search Testimonials',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Testimonials post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-megaphone',
			 'rewrite'	      => array( 'slug' => 'Testimonial', 'with_front' => false ),
			 'has_archive'      => 'Testimonials',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'author', 'thumbnail', 'revisions')
		)
	);
}

// Changes text on title input box in admin
add_filter('gettext','custom_testimonial_title');
function custom_testimonial_title( $input ) {
    global $post_type;
    if( is_admin() && 'Enter title here' == $input && 'testimonial' == $post_type )
        return "Person's Name";
    return $input;
}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_producttestimonialinfo',
	'title' => 'Testimonial Information',
	'fields' => array (
		array (
			'key' => 'field_producttestimonialtag',
			'label' => "Person's Role (or Tagline)",
			'name' => 'product_testimonial_tagline',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
		),
		array (
			'key' => 'field_producttestimonialimage',
			'label' => "Person's Image",
			'name' => 'product_testimonial_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_product_testimonial',
			'label' => 'Testimonial',
			'name' => 'testimonial',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_56145d7fcce6c',
			'label' => 'Products (to associate testimonial with)',
			'name' => 'products',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array (
				0 => 'product',
			),
			'taxonomy' => array (
			),
			'allow_null' => 0,
			'multiple' => 1,
			'return_format' => 'object',
			'ui' => 1,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'testimonial',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;