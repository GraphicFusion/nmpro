<?php

/**
 * Event Custom Post Type
 */
function is_live_event(){
	global $post;
	if( array_key_exists( 'acf' , $_POST ) && array_key_exists( 'field_make_live_event' , $_POST['acf'] ) && $_POST['acf']['field_make_live_event'] == 1){
		return true;
	}
	if( array_key_exists('is_live',$_GET)){
		return true;
	}
	else{
		$event_id = "";
		if( array_key_exists('post', $_GET ) ){
			$event_id = $_GET['post'];
		}
		elseif( is_object( $post ) && 'event' == $post->post_type ){			
			$event_id = $post->ID;
		}
		if( $event_id ){
			if( get_field( 'make_live_event', $event_id)){
				return true;
			}
			else{
				return false;
			}
		}	
		else{
			return false;
		}		
	}
}
add_action( 'init', 'register_event_post_type');
function register_event_post_type()
{ 
	if( is_live_event() ){
		$add_new_item = "Add a New Live Event";
		$edit_item = "Edit Live Event";
	}
	else{
		$add_new_item = "Add a New Event";
		$edit_item = "Edit Event";
	}
	register_post_type( 'event',
		array( 'labels' => 
			array(
				'name'               => 'Events',
				'singular_name'      => 'Event',
				'all_items'          => 'Events',
				'add_new'            => 'Add New',
				'add_new_item'       => $add_new_item,
				'edit'               => 'Edit',
				'edit_item'          =>  $edit_item,
				'new_item'           => 'New Event',
				'view_item'          => 'View Event',
				'search_items'       => 'Search Events',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Events post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-calendar',
			 'rewrite'	      => array( 'slug' => 'event', 'with_front' => false ),
			 'has_archive'      => 'events',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'author', 'thumbnail', 'revisions')
		)
	);

}
/**
 * Event Categories
 */
register_taxonomy( 'event_cat', 
	array('event'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Event Categories',
			'singular_name' 	=> 'Event Category',
			'search_items' 		=> 'Search Event Categories',
			'all_items' 		=> 'All Event Categories',
			'parent_item' 		=> 'Parent Event Category',
			'parent_item_colon' => 'Parent Event Category:',
			'edit_item' 		=> 'Edit Event Category',
			'update_item' 		=> 'Update Event Category',
			'add_new_item' 		=> 'Add New Event Category',
			'new_item_name' 	=> 'New Event Category Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'event-cat' ),
	)
);

// Changes text on title input box in admin
add_filter('gettext','custom_enter_title');
function custom_enter_title( $input ) {
    global $post_type;
    if( is_admin() && 'Enter title here' == $input && 'event' == $post_type )
        return 'Event Name';
    return $input;
}

if( function_exists('acf_add_local_field_group') ){
	$allzones = timezone_identifiers_list();	
	$settings_fields[] = array (
		'key' => 'field_globaltimezone',
		'label' => 'Time Zones',
		'name' => 'global_timezones',
		'type' => 'repeater',
		'instructions' => 'Your events will label their times as GMT unless a timezone is set for that specific event. Here, you can build your list of available timezones.',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'min' => '',
		'max' => '',
		'layout' => 'table',
		'button_label' => 'Add Location',
		'sub_fields' => array (
			array (
				'key' => 'field_test85s',
				'label' => 'Location',
				'name' => 'global_timezone',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => $allzones,
				'default_value' => array (),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'placeholder' => '',
				'disabled' => 0,
				'readonly' => 0,
			),
		)
	);
	acf_add_local_field_group(array (
		'key' => 'group_globalfc2174',
		'title' => 'Settings',
		'fields' => $settings_fields,
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'site-options-events',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));


	$fields = array();
	$agenda_fields = array();
	$agenda_fields[] = array (
		'key' => 'field_agendasubtitle',
		'label' => 'Agenda Subtitle (optional)',
		'name' => 'agenda_subtitle',
		'type' => 'text',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => '',
		'prepend' => '',
		'append' => '',
		'maxlength' => '',
		'readonly' => 0,
		'disabled' => 0,
	);

	$agenda_fields = get_agenda_tabs( $agenda_fields );
	if( $settings_zones = get_field('global_timezones','option') ){
		foreach( $settings_zones as $array ){
			$key = $array['global_timezone'];
			$zone_name = $allzones[$key];
			$zones[$key] = $zone_name;
		}
	}
	else{
		$zones = $allzones;
	}

	if( is_admin() ){
		if( !is_live_event() ){
			$fields[] = array (
				'key' => 'field_55edefb62f94d',
				'label' => 'Dates/Time',
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'top',
				'endpoint' => 0,
			);
			$fields[] = array (
				'key' => 'field_55ef14c22934b',
				'label' => 'Set Event Duration (without setting specific dates)',
				'name' => 'duration_override',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '20',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
			);
			$fields[] = array (
				'key' => 'field_55ef14cf2934c',
				'label' => 'Duration of Event (number of days)',
				'name' => 'event_duration',
				'type' => 'number',
				'instructions' => 'If event does not have set dates yet, use this to build the agenda by day. Once dates are set, they will override this.',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55ef14c22934b',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			);
			$fields[] =	array (
				'key' => 'field_55edc057752a2',
				'label' => 'Start Date',
				'name' => 'start_date',
				'type' => 'date_picker',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55ef14c22934b',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => 40,
					'class' => '',
					'id' => '',
				),
				'display_format' => 'F j, Y',
				'return_format' => 'F j, Y',
				'first_day' => 1,
			);
			$voidfields[] = array (
				'key' => 'field_55edc06e752a3',
				'label' => 'Start Time',
				'name' => 'start_time',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55ef14c22934b',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => '15',
					'class' => '',
					'id' => '',
				),
				'choices' => get_time_choices(),
				'default_value' => array (
					'09' => 9,
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'placeholder' => '',
				'disabled' => 0,
				'readonly' => 0,
			);
			$fields[] =	array (
				'key' => 'field_55edc673c9df1',
				'label' => 'End Date',
				'name' => 'end_date',
				'type' => 'date_picker',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55ef14c22934b',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => 40,
					'class' => '',
					'id' => '',
				),
				'display_format' => 'F j, Y',
				'return_format' => 'F j, Y',
				'first_day' => 1,
			);
			$voidfields[] =	array (
				'key' => 'field_55edc684c9df2',
				'label' => 'End Time',
				'name' => 'end_hours',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55ef14c22934b',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => '15',
					'class' => '',
					'id' => '',
				),
				'choices' => get_time_choices(),
				'default_value' => array (
					'04' => 4,
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'placeholder' => '',
				'disabled' => 0,
				'readonly' => 0,
			);
			$fields[] = array (
				'key' => 'field_55edcaba9863f',
				'label' => 'End Timestamp',
				'name' => 'end_timestamp',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55edc06e752a3',
							'operator' => '==',
							'value' => '01',
						),
						array (
							'field' => 'field_55edc06e752a3',
							'operator' => '!=',
							'value' => '01',
						),
					),
				),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
				'readonly' => 0,
				'disabled' => 0,
			);
			$fields[] =	array (
				'key' => 'field_55edda45a49c9',
				'label' => 'Start Timestamp',
				'name' => 'start_timestamp',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55edc06e752a3',
							'operator' => '==',
							'value' => '01',
						),
						array (
							'field' => 'field_55edc06e752a3',
							'operator' => '!=',
							'value' => '01',
						),
					),
				),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
				'readonly' => 0,
				'disabled' => 0,
			);
		}
		else{
			$fields[] = array (
				'key' => 'field_55edefb62f94d',
				'label' => 'Live Event Information',
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'top',
				'endpoint' => 0,
			);
			$fields[] =	array (
				'key' => 'field_55edc057752a2',
				'label' => 'Event Date',
				'name' => 'start_date',
				'type' => 'date_picker',
				'instructions' => '',
				'required' => 0,
				'wrapper' => array (
					'width' => 40,
					'class' => '',
					'id' => '',
				),
				'display_format' => 'F j, Y',
				'return_format' => 'F j, Y',
				'first_day' => 1,
			);
	
			$fields[] = array (
				'key' => 'field_55edc06e752a3',
				'label' => 'Start Time',
				'name' => 'start_time',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'wrapper' => array (
					'width' => '25',
					'class' => '',
					'id' => '',
				),
				'choices' => get_time_choices(),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'placeholder' => '',
				'disabled' => 0,
				'readonly' => 0,
			);
			$fields[] = array (
				'key' => 'field_timezonea98634',
				'label' => 'Time Zone <a style="float:right;" href="/wp-admin/admin.php?page=site-options-events">Add a timezone</a>',
				'name' => 'timezone',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '35',
					'class' => '',
					'id' => '',
				),
				'choices' => $zones,
				'default_value' => array (
					'pm' => 'pm',
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'ajax' => 0,
				'placeholder' => '',
				'disabled' => 0,
				'readonly' => 0,
			);
			$fields[] =	array (
				'key' => 'field_l55edda45a49c9',
				'label' => 'Start Timestamp',
				'name' => 'start_timestamp',
				'type' => 'number',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'field_55edc06e752a3',
							'operator' => '==',
							'value' => '01',
						),
						array (
							'field' => 'field_55edc06e752a3',
							'operator' => '!=',
							'value' => '01',
						),
					),
				),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
				'readonly' => 0,
				'disabled' => 0,
			);
			$fields[] = array (
				'key' => 'field_streaming_code',
				'label' => 'Livestream Code',
				'name' => 'streaming_code',
				'wrapper' => array (
					'width' => '99',
					'class' => '',
					'id' => '',
				),
				'type' => 'textarea',
				'button_label' => 'Add The Video Streaming Code',
			);
			$fields[] = array (
				'key' => 'field_event_code',
				'label' => 'Group IDs (separated by commas)',
				'name' => 'event_code',
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
				'type' => 'text',
				'button_label' => 'Add The Video Streaming Code',
			);
		}
	}
	else{
		$fields[] = array (
			'key' => 'field_55ef14c22934b',
			'label' => 'Set Event Duration (without setting specific dates)',
			'name' => 'duration_override',
			'type' => 'true_false',
			'default_value' => 0,
		);
		$fields[] = array (
			'key' => 'field_55ef14cf2934c',
			'label' => 'Duration of Event (number of days)',
			'name' => 'event_duration',
			'type' => 'number',
		);
		$fields[] =	array (
			'key' => 'field_55edc057752a2',
			'label' => 'Start Date',
			'name' => 'start_date',
			'type' => 'date_picker',
			'instructions' => '',
			'required' => 0,
			'display_format' => 'F j, Y',
			'return_format' => 'F j, Y',
			'first_day' => 1,
		);
		$fields[] =	array (
			'key' => 'field_55edc673c9df1',
			'label' => 'End Date',
			'name' => 'end_date',
			'type' => 'date_picker',
			'instructions' => '',
			'required' => 0,
			'display_format' => 'F j, Y',
			'return_format' => 'F j, Y',
			'first_day' => 1,
		);
		$fields[] = array (
			'key' => 'field_55edcaba9863f',
			'label' => 'End Timestamp',
			'name' => 'end_timestamp',
			'type' => 'number',
		);
		$fields[] =	array (
			'key' => 'field_55edda45a49c9',
			'label' => 'Start Timestamp',
			'name' => 'start_timestamp',
			'type' => 'number',
		);
		$fields[] = array (
			'key' => 'field_55edc06e752a3',
			'label' => 'Start Time',
			'name' => 'start_time',
			'type' => 'select',
			'choices' => get_time_choices(),
		);
		$fields[] = array (
			'key' => 'field_timezonea98634',
			'label' => 'Time Zone <a style="float:right;" href="/wp-admin/admin.php?page=site-options-events">Add a timezone</a>',
			'name' => 'timezone',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'choices' => $zones,
			'default_value' => array (
				'pm' => 'pm',
			),
		);
			$fields[] = array (
				'key' => 'field_product_ids',
				'label' => 'Product Id',
				'name' => 'is_product_ids',
				'type' => 'repeater',
				'layout' => 'table',
				'button_label' => 'Add An Infusionsoft Product Id',
				'sub_fields' => array (
					array (
						'key' => 'field_product_id',
						'label' => 'Product Id',
						'name' => 'is_product_id',
						'type' => 'text',
					),
				)
			);
		$fields[] =	array (
			'key' => 'field_product_url',
			'label' => 'Product Url',
			'name' => 'is_product_url',
			'type' => 'text',
		);

	}
	$fields[] = array (
		'key' => 'field_packagestab',
		'label' => 'Price Packages',
		'name' => '',
		'type' => 'tab',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'placement' => 'top',
		'endpoint' => 0,
	);
$fields[] = array (
	'key' => 'field_price_package_bg',
	'label' => 'Background Image',
	'name' => 'price_package_background_image',
	'type' => 'image',
	'return_format' => 'array'
);
	$fields[] = array (
		'key' => 'field_packageintrotext',
		'label' => 'Pricing Introduction',
		'name' => 'package_introduction',
		'type' => 'wysiwyg',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'tabs' => 'all',
		'toolbar' => 'full',
		'media_upload' => 1,
	);
	$fields[] =	array (
		'key' => 'field_5613df9926257',
		'label' => 'Price Packages',
		'name' => 'event_price_packages',
		'type' => 'repeater',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'min' => '',
		'max' => '',
		'layout' => 'table',
		'button_label' => 'Add Row',
		'sub_fields' => array (
			array (
				'key' => 'field_5613dfad26258',
				'label' => 'Package Text',
				'name' => 'package_text',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
			),
			array (
				'key' => 'field_5613dfb726259',
				'label' => 'InfusionSoft Link',
				'name' => 'package_link',
				'type' => 'url',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
			),
		),
	);
	$fields[] = array (
		'key' => 'field_landingpage94e',
		'label' => 'Landing Page',
		'name' => '',
		'type' => 'tab',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'placement' => 'top',
		'endpoint' => 0,
	);
	$fields[] = $events_landing_layouts;
	$fields[] = array (
		'key' => 'field_eventExcerpt',
		'label' => 'Excerpt',
		'name' => 'landing_excerpt',
		'type' => 'textarea',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'default_value' => '',
		'placeholder' => '',
		'prepend' => '',
		'append' => '',
		'maxlength' => '',
		'readonly' => 0,
		'disabled' => 0,
	);
	$fields[] = array (
		'key' => 'field_55edeff42f94e',
		'label' => 'Settings',
		'name' => '',
		'type' => 'tab',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'placement' => 'top',
		'endpoint' => 0,
	);
	$voidfields[] = array(
		'key' => 'field_55ee33a8a7f49',
		'label' => 'Mssg',
		'name' => '',
		'type' => 'message',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '',
			'class' => '',
			'id' => '',
		),
		'message' => 'Please do this',
		'esc_html' => 0,
	);
	$fields[] = array (
		'key' => 'field_55edcaba98634',
		'label' => 'Start Days at: ',
		'name' => 'start_agendas_at',
		'type' => 'select',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '30',
			'class' => '',
			'id' => '',
		),
		'choices' => get_time_choices( 60*60, 0, 23*60*60 ),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'placeholder' => '',
		'disabled' => 0,
		'readonly' => 0,
	);
	$fields[] = array (
		'key' => 'field_55edcaba98des',
		'label' => 'End Days at: ',
		'name' => 'end_agendas_at',
		'type' => 'select',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '30',
			'class' => '',
			'id' => '',
		),
		'choices' => get_time_choices( 60*60, 0, 23*60*60 ),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'placeholder' => '',
		'disabled' => 0,
		'readonly' => 0,
	);
	$fields[] = array (
		'key' => 'field_55edbeifa98634',
		'label' => 'Schedule Increment ',
		'name' => 'schedule_increment',
		'type' => 'select',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '30',
			'class' => '',
			'id' => '',
		),
		'choices' => array (
			'300' => '5 min',
			'600' => '10 min',
			'900' => '15 min',
			'1800' => '30 min',
			'3600' => '60 min',
		),
		'default_value' => array (
			'pm' => 'pm',
		),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'placeholder' => '',
		'disabled' => 0,
		'readonly' => 0,
	);
	$fields[] =	array (
		'key' => 'field_55edf0062f94f',
		'label' => 'Event is active',
		'name' => 'event_active',
		'type' => 'true_false',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array (
			'width' => '10',
			'class' => '',
			'id' => '',
		),
		'message' => '',
		'default_value' => 1,
	);
	if( !is_live_event()){
		$fields[] = array (
			'key' => 'field_timezonea98634',
			'label' => 'Time Zone <a style="float:right;" href="/wp-admin/admin.php?page=site-options-events">Add a timezone</a>',
			'name' => 'timezone',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '30',
				'class' => '',
				'id' => '',
			),
			'choices' => $zones,
			'default_value' => array (
				'pm' => 'pm',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		);
	}
	// checks to see if we're hitting the admin page from the "Add New Live Event" link
	if( is_live_event()){
		$make_live = true;
	}
	else{
		$make_live = false;
	}
	$fields[] = array (
		'key' => 'field_make_live_event',
		'label' => 'Live Event',
		'name' => 'make_live_event',
		'type' => 'true_false',
		'instructions' => '',
		'required' => 0,
		'default_value' => $make_live
	);
	if( !is_live_event() ){
		$fields[] = array (
			'key' => 'field_locaeff42f94e',
			'label' => 'Location',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		);
		$fields[] = array (
			'key' => 'field_eventLocationText',
			'label' => 'Event Location',
			'name' => 'event_location_text',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		);
		$fields[] = array (
			'key' => 'field_group_code',
			'label' => 'Group Code',
			'name' => 'event_group_code',
			'type' => 'text',
		);
		$fields[] = array (
			'key' => 'field_locat30cb5851',
			'label' => 'Locations</a>',
			'name' => 'event_locations',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => '',
			'max' => '1',
			'layout' => 'table',
			'button_label' => 'Add Location',
			'sub_fields' => array (
				array (
					'key' => 'field_55edflocat85s',
					'label' => 'Location',
					'name' => 'event_location',
					'type' => 'post_object',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'post_type' => array (
						0 => 'location',
					),
					'taxonomy' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'return_format' => 'object',
					'ui' => 1,
				),
			),
		);
	}
	acf_add_local_field_group(array (
		'key' => 'group_55edc04fc2174',
		'title' => 'Information',
		'fields' => $fields,
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'event',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	if( is_array( $agenda_fields ) && !is_live_event()){
		acf_add_local_field_group(array (
			'key' => 'group_55edthisc2174',
			'title' => 'Agenda',
			'fields' => $agenda_fields,
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'event',
					),
				),
			),
			'menu_order' => 1,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}
};

// build an array of times for select dropdowns based on start & end times and increments (in event settings ) 
function get_time_choices( $increment = null, $i = null, $end = null){
	if( array_key_exists( 'post', $_GET) && is_numeric($_GET['post'])){
		$post_id = $_GET['post'];
		if( !$increment ){
			$increment = get_field( 'schedule_increment', $post_id);
		}
		if(!$i){
			$i = get_field( 'start_agendas_at',$post_id);
		}
		if( !$end) {
			$end = get_field( 'end_agendas_at',$post_id);
		}
	}
	if( !$increment ){	$increment = 60*60; }
	if( !isset($i) ){ $i = 18000; }  
	if( !$end ){ $end = 23*60*60; }
	while ( $i <= $end ){ 
		$hours = floor($i / 3600);
		$mins = str_pad(floor(($i - ($hours*3600)) / 60), 2, '0', STR_PAD_LEFT);
		$ampm = " AM";
		if( $hours >= 12 ){
			if( $hours < 24 ){
				$ampm = " PM";
			}
			if( $hours >= 13 ){
				$hours = $hours -12;
			}
		}
		else{
			if( $hours == 0 ){
				$hours = 12;
			}
		}
		$time_choices[ $i ] = $hours.":".$mins.$ampm;
		$i = $i + $increment;
	}
	return $time_choices; 
}
function get_event_duration( $post ){
	$duration = 1;
	if( get_field('duration_override', $post->ID)){
		$duration = get_field('event_duration', $post->ID);
	}
	else{
		$start_date = strtotime(get_field('start_date', $post->ID));
		$end_date = strtotime( get_field('end_date', $post->ID));
		if( $start_date && $end_date ){
			$duration = ($end_date - $start_date)/DAY_IN_SECONDS + 1; // difference plus one 
		}
	}	
	return $duration;
}
// get_agenda_tabs will register multiple agenda tabs; 
function get_agenda_tabs( $fields ){
	$i = 1;
	$duration = 1; // provides at least one day for agenda for ease of use
	// checks to see if is admin page
	if( isset( $_GET ) && array_key_exists( 'post', $_GET) ){
		$post = get_post( $_GET['post'] );
		// checks to see if is event admin page
		if( is_object( $post ) && 'event' == $post->post_type ){
			// checks if duration is set by number of days (override) or by dates 
			$duration = get_event_duration( $post );
		}
	}
	elseif( isset( $_POST ) && array_key_exists( 'post_type', $_POST) && $_POST['post_type'] == 'event' ) {
			$event = get_post( $_POST['post_ID'] );
			$duration = get_event_duration( $event );
			$max_duration = get_transient('max_agenda_duration');
			if( $duration > $max_duration ){
				 set_transient( 'max_agenda_duration', $duration );
			}
	}
	elseif( !is_admin() ){
		$duration = get_transient('max_agenda_duration');
	}
	if( $duration > 0 ){ // no negative durations
		while( $i <= $duration  ){
			$fields[] = array (
				'key' => 'field_55edf06c1a55'.$i,
				'label' => 'Day '.$i,
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'top',
				'endpoint' => 0,
			);
			$fields[] = array (
				'key' => 'field_55edf0801a55'.$i,
				'label' => 'Agendas',
				'name' => 'event_agendas'.$i,
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'min' => '1',
				'max' => '',
				'layout' => 'block',
				'button_label' => 'Add Agenda Item',
				'sub_fields' => array (
					array (
						'key' => 'field_agendaName',
						'label' => 'Name',
						'name' => 'agenda_name',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => 80,
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_startTime',
						'label' => 'Start Time',
						'name' => 'agenda_time',
						'type' => 'select',
						'instructions' => '			',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '20',
							'class' => '',
							'id' => '',
						),
						'choices' => get_time_choices(),
						'default_value' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'ajax' => 0,
						'placeholder' => '',
						'disabled' => 0,
						'readonly' => 0,
					),
					array (
						'key' => 'field_agendaSpeakers'.$i,
						'label' => 'Speakers',
						'name' => 'agenda_speakers'.$i,
						'type' => 'repeater',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => 'Add Speaker',
						'sub_fields' => array (
							array (
								'key' => 'field_sagendaspeakerfb585',
								'label' => 'Speaker',
								'name' => 'agenda_speaker',
								'type' => 'post_object',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'post_type' => array (
									0 => 'speaker',
								),
								'taxonomy' => array (
								),
								'allow_null' => 0,
								'multiple' => 0,
								'return_format' => 'object',
								//'ui' => 1,
							),
						),
					),
				)
			);
			$i++;
		}
		return $fields;
	}
}

function get_timestamp( $array ){
	if( array_key_exists( 'date', $array ) ){
		$date = DateTime::createFromFormat('Ymd', $array['date'] );
		if( is_object( $date ) ){
			$datestamp = strtotime( $date->format('d-m-Y') );
			if( array_key_exists( 'time', $array ) ){
				$datestamp = $datestamp + $array['time'];
			}
			return $datestamp;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
}
// add timestamp to hidden acf field when post is saved - makes search/sort funcitonality in mysql much easier
add_action( 'save_post', 'add_event_timestamp' );
function add_event_timestamp( $post_id ) {

	$post = get_post( $post_id );

	if( !empty ( $_POST ) && array_key_exists('post_type',$_POST)){
		if ( 'event' != $_POST['post_type'] ) {
	        return;
	    }
		$start = array();
		$end = array();
		if( array_key_exists( 'acf' , $_POST ) ){
			if( array_key_exists( 'field_55edc057752a2' , $_POST['acf'] ) ){
				$start['date'] = $_POST['acf']['field_55edc057752a2'];
			}
			if( array_key_exists( 'field_55edc673c9df1' , $_POST['acf'] ) ){
				$end['date'] = $_POST['acf']['field_55edc673c9df1'];
			}
			if( array_key_exists( 'field_55edc06e752a3' , $_POST['acf'] ) ){
				$start['time'] = $_POST['acf']['field_55edc06e752a3'];
			}
			$start_timestamp = get_timestamp( $start );

			$end_timestamp = get_timestamp( $end );
			if( $start_timestamp ){
				update_post_meta ( $_POST['post_ID'] , 'start_timestamp' , $start_timestamp );
			}
			else{
				delete_post_meta ( $_POST['post_ID'] , 'start_timestamp' );
			}
			if( $end_timestamp ){
				update_post_meta ( $_POST['post_ID'] , 'end_timestamp' , $end_timestamp );
			}
			else{
				delete_post_meta ( $_POST['post_ID'] , 'end_timestamp' );
			}
		}
	}
}

/**
  * EVENTS Class 
  *
  * Returns all events, recurring and fixed, sorted chronologically from today
  * 
  * @start_date string 	optional - this marks the beginning of the time frame of interest in the form 'YYYYMMDD'; default is current time;
  * @max_limit integer 	optional - number of days to search from $start_date
  * @event_cat string 	optional - category slug to limit search to
  * 
  * @return array of key = event's timestamp and value = array of event's post id; array is sorted with recurring events populated for each occurence. 
  * 			Array(
  *				    [1409529600] => Array
  *				        (
  *				            [0] => 299
  *				        )
  *				
  *				    [1410912000] => Array
  *				        (
  *				            [0] => 296
  *				            [1] => 298
  *				        )
  *
  *					)
  * 
  */
class doradoEvents{

	function get_events( $start_date = NULL, $max_limit = 0, $event_cat = NULL , $max_number = null , $includeEvents = null ){
		global $wpdb;
		if( !$start_date ){ $start_date = current_time('timestamp'); }
		$max_time = $start_date + $max_limit * 86400;
		$single_events = array();
		$recurring_events = array();
		$total_events = 0; 
		$args = array(
			'numberposts' => -1,
			'post_type' => 'event',
			'meta_query' => array(
				array(
					'key' => 'start_timestamp',
					'value' => $start_date,
					'type' => 'NUMERIC',
					'compare'=> '>='				
				),
				array(
					'key' => 'event_active',
					'value' => 1,
					'type' => 'NUMERIC',
					'compare'=> '=='				
				)
			)
		);
		if($max_limit ){
			$args['meta_query'][] = 	
				array(
					'key' => 'start_timestamp',
					'value' => $max_time,
					'type' => 'NUMERIC',
					'compare'=> '<'
				);
		}
		if($event_cat){ 
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'event_cat',
					'field' => 'slug',
					'terms' => $event_cat
				)
			);	
			
		}
		if($includeEvents){
			$args['post__in'] = $includeEvents;
		}
		$the_query = new WP_Query( $args );
		if( $the_query->have_posts() ){
			foreach ( $the_query->posts as $spost ) {
				$count = 0;
				if( $eventstamp = get_field('start_timestamp', $spost->ID ) ){
					$single_events[$eventstamp][] = array('post'=> $spost->ID );
					$count++;
					$total_events++;
				}
			}
		}
/*		if( $max_limit > 1 ){
			$meta_query = array( 'key' => 'recurring_day'); 
		}
		else{
			$meta_query = array(
							'key' => 'recurring_day',
							'value' => date ( 'w' , $stamp),
							'compare' => '=',
							'type' => 'numeric',
				       );
		}		
		$recurring_query = new WP_Query( array (
						    'post_type' => 'event',
						    'meta_key' => 'recurring_day',
						    'orderby' => 'meta_value_num',
						    'order' => 'ASC',
						    'meta_query' => array( $meta_query )
						));	
		if( $max_limit > 1 ){
			// $recurring count is the number of times the recurring day occurs in the period of interest
			$recurring_count = floor($max_limit/ 7);
			foreach($recurring_query->posts as $re_key => $re_post){
				$recurring_day = get_field( 'recurring_day', $re_post->ID );
				$dif_num = $recurring_day - date('w');
				if( $dif_num < 0 ){ $dif_num = $dif_num + 7;}
				$i = 0;		
				while($i < $recurring_count){
					$recurring_timestamp = strtotime( 'today 12:00am +' . $dif_num .' day');
					$recurring_events[$recurring_timestamp][] = $re_post->ID;
					$dif_num = $dif_num +7;
					$i++;
					$total_events++;
				}			
			}
		}
		else{
			foreach($recurring_query->posts as $re_key => $re_post){
				$recurring_day = get_field( 'recurring_day', $re_post->ID );
				$recurring_events[$stamp][] = $re_post;
				$total_events++;					
			}	
		}
		$all_events = $single_events + $recurring_events;*/
		$all_events = $single_events;
		ksort($all_events);
		if (count($single_events)>0 && $max_number > 0){
			$i = 0;
			while($i<$max_number){
				foreach( $all_events as $timestamp=>$event_array){
					foreach( $event_array as $key=>$event_id ){
						if($i < $max_number){
							$new_array[$timestamp][] = $event_id;
							if($i == $total_events -1 ){ $i = $max_number; }							
							$i++;
						}
					} 
				}
			
			}
			$all_events = $new_array;
		}
		$this->events = $all_events;
	}
}
function showEventBlock( $eventObj ){
	// Event Block will grab the FIRST category; only set one
	$id = $eventObj->ID;
	$terms = get_the_terms( $id, 'event_cat' );
	$cat = $terms[0];
	if( have_rows('dorado-events' , $id) ): $i = 0;
	    while ( have_rows('dorado-events', $id) && !$i ) : the_row();	
	        // display a sub field value
	        $date = strtotime( get_sub_field('dorado_event_date') ); // convert date to unix; output by acf as Ymd: 20150618
			if ( $date ){
				$date = date( 'F j, Y' , $date);
			}
	        $time = the_sub_field('dorado_event_time');
			$i = 1;
	    endwhile;	
	endif;
?>
	<div class='event-block'>
		<div class='label-block'><?php echo $cat->name; ?></div>
		<div class='content-block'>
			<div class='overlay-wrapper'>
				<div class='overlay-bg' style="background:url(<?php $bgArray = get_field('featured-event-image', $id); echo $bgArray['url']; ?>)"></div>
				<div class='overlay'></div>
				<div class='overlay-content'>
					<label><?php echo $eventObj->post_title; ?></label>
					<div class='date'>
						<?php echo $date; ?>			
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}
/**
 * FILTERS
 **/

add_filter('rewrite_rules_array', 'add_event_rewrite_rules');
add_filter('query_vars', 'add_event_query_vars');
  
// Adds a url query to pass date requests in the URL (i.e., example.net/?var1=value1&calendar_date=2014-08
function add_event_query_vars($aVars) {
	$aVars[] = "event_page";
	$aVars[] = "search_terms";
	return $aVars;
}

// ON INSTALL SAVE Permalinks or flush_rewrite_rules();
function add_event_rewrite_rules($aRules) {
	$aNewRules = array('events/page/?([^/]*)/?([^/]*)' => 'index.php?post_type=event&event_page=$matches[1]&search_terms=$matches[2]');
	$aRules = $aNewRules + $aRules;
	return $aRules;
}
//add_filter( 'redirect_canonical','event_disable_redirect_canonical' );
function event_disable_redirect_canonical( $redirect_url ){
    global $post;
    $ptype = get_post_type( $post );
    if ( $ptype == 'event' && is_archive() ) $redirect_url = false;
    return $redirect_url;
}
function get_event_agenda( $post ){
	if( property_exists( $post, 'duration' ) ){
		$duration = $post->duration;
	}
	else{
		$duration = get_event_duration( $post );
	}
	$id = $post->ID;
	$incr = 60*60*24;
	$start = 0; 
	if( property_exists( $post, 'start_timestamp' ) ){
		$start = $post->start_timestamp;
	}
	$days = array();
	//iterate over custom fields by day:
	for( $i = 1; $i <= $duration; $i++ ){
		$stamp = $i*$incr;
		$agendas = array();
		if( have_rows('event_agendas'.$i , $id) ){
			$agendas = array();
		    while ( have_rows('event_agendas'.$i , $id) ) { the_row();
		        $agenda_name = get_sub_field('agenda_name');
		        $agenda_time = get_sub_field('agenda_time');
		        $agenda_speakers = get_sub_field('agenda_speakers'.$i);
				if( is_numeric( $agenda_time ) ){
					$agendas[$agenda_time] = array( 'name'=>$agenda_name, 'speakers'=>$agenda_speakers);
				}
			}
		}
		$days[$stamp] = $agendas;
	}
	return $days;
}
function get_event( $post ){
	//EVENT INFO
	$event = new stdClass();
	$event->ID = $post->ID;
	$event->title = $post->post_title;
	$event->location = get_field('event_location_text', $post->ID);	
	$event->group_code = get_field('event_group_code', $post->ID);	
	$event->start_timestamp = get_field("start_timestamp",$post->ID);
	$event->start_month = date( 'F', $event->start_timestamp);
	$event->start_day = date( 'jS', $event->start_timestamp);
	$event->end_timestamp = get_field("end_timestamp",$post->ID);
	$event->end_month = date( 'F', $event->end_timestamp);
	$event->end_day = date( 'jS', $event->end_timestamp);
	$event->duration = get_event_duration( $event );
	$event->agenda = get_event_agenda( $event );
	return $event;
}
