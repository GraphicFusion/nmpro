<?php

/**
 * Video Custom Post Type
 */
add_action( 'init', 'register_video_post_type');
function register_video_post_type()
{ 
	register_post_type( 'video',
		array( 'labels' => 
			array(
				'name'               => 'Videos',
				'singular_name'      => 'Video',
				'all_items'          => 'All Videos',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Video',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Video',
				'new_item'           => 'New Video',
				'view_item'          => 'View Video',
				'search_items'       => 'Search Videos',
				'not_found'          => 'Nothing found in the Database.',
				'not_found_in_trash' => 'Nothing found in Trash',
				'parent_item_colon'  => ''
			),
			'description'         => 'Videos post type',
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 10,
			'menu_icon'           => 'dashicons-video-alt',
			 'rewrite'	      => array( 'slug' => 'video', 'with_front' => false ),
			 'has_archive'      => 'videos',
			'capability_type'     => 'page',
			'hierarchical'        => true,
			'supports'            => array( 'title', 'author', 'thumbnail', 'revisions')
		)
	);
}

/**
 * Video Categories
 */
register_taxonomy( 'video_cat', 
	array('video'),
	array('hierarchical' => true,
		'labels' => array(
			'name' 				=> 'Video Categories',
			'singular_name' 	=> 'Video Category',
			'search_items' 		=> 'Search Video Categories',
			'all_items' 		=> 'All Video Categories',
			'parent_item' 		=> 'Parent Video Category',
			'parent_item_colon' => 'Parent Video Category:',
			'edit_item' 		=> 'Edit Video Category',
			'update_item' 		=> 'Update Video Category',
			'add_new_item' 		=> 'Add New Video Category',
			'new_item_name' 	=> 'New Video Category Name',
		),
		'show_admin_column' => true, 
		'show_ui' 			=> true,
		'query_var' 		=> true,
		'rewrite' 			=> array( 'slug' => 'video-cat' ),
	)
);


// not being used; experiment
function get_youtube_stats( $id ){
	$statsUrl = 'https://www.googleapis.com/youtube/v3/videos?id='.$id.'&key=AIzaSyByHIG6TC4C5qAY4LilSCNWAAg6mLCK0Bc&part=statistics';
	$stats=@file_get_contents( $statsUrl );
	$statsObj=json_decode($stats);
	$video['views'] = $statsObj->items[0]->statistics->viewCount;
	$video['likes'] = $statsObj->items[0]->statistics->likeCount;
	return $video;
}

// use this when saving a video cpt and save duration ?? and popularity?
function get_youtube_data($video_id){
echo $video_id;

	$dataUrl = 'https://www.googleapis.com/youtube/v3/videos?id='.$video_id.'&key=AIzaSyByHIG6TC4C5qAY4LilSCNWAAg6mLCK0Bc&part=contentDetails';

	$data=@file_get_contents( $dataUrl );
	if (false===$data) return false;
	$obj=json_decode($data);
	$isoduration = $obj->items[0]->contentDetails->duration;
	$duration = preg_replace( '/PT/','',$isoduration );
	$duration = preg_replace( '/M/',' Minutes ',$duration );
	$duration = preg_replace( '/S/',' Seconds',$duration );
	$duration = strtotime( $duration , 0 );
	$video['duration'] = $duration;
	return $video;
}
function add_youtube_meta( $post_id ) {
	$post = get_post( $post_id );
	if( !empty ( $_POST ) && array_key_exists('post_type',$_POST)){
		if ( 'video' != $_POST['post_type'] ) {
	        return;
	    }
		if( array_key_exists( 'acf' , $_POST ) ){
			if( array_key_exists( 'field_55f2007a572de' , $_POST['acf'] ) ){
				$url = $_POST['acf']['field_55f2007a572de'];
				$data = get_youtube_data( $url );
				if( $_POST['acf']['field_55f2007a572de'] && empty($_POST['acf']['field_duration572e2'])){
					
					$duration = $data['duration'];
					if( $duration > 0 ){
						update_post_meta ( $_POST['post_ID'] , 'video_duration' , $duration );
					}
				}
			}
		}
	}
}
add_action( 'save_post', 'add_youtube_meta' );

add_action('in_admin_footer', 'videos_in_foot');
function videos_in_foot(){
	$videos = get_videos( 'category' );
	echo "<script>var videos = ".json_encode( $videos ).";
	function updateVideos( acfType , el ){
		var chosen = $(el).find('.select2-chosen').html();
		if( acfType == '.acf-row' && !$(el).closest( acfType ).length){
			acfType = '.acf-field';
			var video_select = $(el).closest( acfType ).next('.videos-select').find('select');
		}
		else{
			var video_select = $(el).closest( acfType ).find('.videos-select select');
		}
		var optionStr = '';
		$.each( videos[chosen] , function(m,v){
			optionStr += '<option value=\"' + v.post_id + '\">' + v.post_title + '</option>';
		});
		$(video_select).html(optionStr);		
	}
	function bindVideoSelect(){
		$('.video-cat-select').on('change', function(e){
			var el = this;
			updateVideos('.acf-row', el);
		});			
		$('.video-cat-select-single').on('change', function(e){
			var el = this;
			updateVideos('.acf-fields', el)
		});				
	}
	jQuery(document).ready(function($) { 
		if( 'undefined' != typeof acf ){
			bindVideoSelect();
			acf.add_action('append', function( ){
				bindVideoSelect();
			});
		}
	});

</script>";
}
function get_videos(  $arrange = NULL, $terms = array() ){
	if( count( $terms ) < 1 ){
		$terms = get_terms( 'video_cat');
	}
	foreach( $terms as $term ){
		$tax_terms[] = $term->slug;
	}
	$args=array(
		'post_type' => 'video',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'tax_query' => array(
			array(
				'taxonomy' => 'video_cat',
				'field'		=> 'slug',
				'terms' => $tax_terms
			)
		)
	);
	$query = new WP_Query($args);
	if( $arrange == 'category' && $query->have_posts() ){
		foreach( $query->posts as $video ){
			$terms = get_the_terms( $video->ID, 'video_cat');
			foreach( $terms as $term ){
				$videos[$term->name][] = array( 'post_id'=>$video->ID, 'post_title'=>$video->post_title );
			}
		}
	}
	else{
		foreach( $query->posts as $post ){
			$videos[$post->ID] = $post->post_title;
		}
	}
	return $videos;
}
function video_select_field( $field )
{
	$field['choices'] = get_videos();
	return $field;
}
// acf/load_field/name={$field_name} - filter for a specific field based on it's name
add_filter('acf/load_field/name=video', 'video_select_field');

function get_video_meta( $video_post ){
	if( is_object($video_post) ){
		$video_id = $video_post->ID;
		$v = $video_post;
		$v->title = $v->post_title;
		$v->a_class = "";
		if( $video_post->cat_slug ){
			$v->cat_slug = $video_post->cat_slug;
		}
		$v->categories = wp_get_object_terms( $video_id, 'video_cat' );
		if( $video_post->category ){
			$v->category = $video_post->category; 
		}
		$v->youtube = get_field('video_youtube_link',$video_id); 
		$v->vimeo = get_field('video_vimeo_link',$video_id); 
		if( $v->youtube ){
			$v->a_class = " youtube-video";
		}
		if( $v->vimeo ){
			$v->a_class = " vimeo-video";
		}
		$v->excerpt = get_field('video_excerpt',$video_id);
		$v->testimonial_name = get_field('video_testimonial_name',$video_id); 
		$duration = get_field('video_duration',$video_id);
		$v->durationText = "";
		if( $duration ){
			$min = floor( $duration/60 );
			$seconds = ( ($duration/60) - $min ) * 60;
			$v->durationText = $min .":" . sprintf("%02d",$seconds);
		}
		$v->background_image = "";
		$video_background_image = get_field('video_background_image',$video_id);
		// check for override; if no override, check for youTube default thumbnail selected. if none selected, use dafualt image;
		if( is_array( $video_background_image ) ){
			$v->background_image = $video_background_image['url'];
		}
		elseif( $youtube_increment = get_field('youtube_image',$video_id) ){
			// youtube_image is a list of four possible thumbnails; if one is chosen, it is grabbed here as background image
			if( $youtube_increment == 4 ){ $youtube_increment = '0'; } // little hack to use ZERO as false;
			if( $youtube_increment == 5 ){ $youtube_increment = 'maxresdefault'; } // little hack to use ZERO as false;
			$youtube_id = $v->youtube;
			$v->background_image = "http://img.youtube.com/vi/".$youtube_id."/".$youtube_increment.".jpg";
		}
		else{
			$v->background_image = "http://img.youtube.com/vi/".$v->youtube."/maxresdefault.jpg";
//			$v->background_image = get_template_directory_uri() ."/assets/img/default-video.jpg";
		}
		return $v;
	}
}