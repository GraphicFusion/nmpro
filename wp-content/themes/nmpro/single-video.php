<?php
	$v = get_video_meta( $post );  // see lib/custom-videos.php
?>
<div class="row cblock video-block">
	<div class="container">
		<?php get_template_part('templates/content', 'single-video'); ?>
	</div>
</div>
<h3>
	<?php if( is_array( $v->categories) ): ?>
		<span class="serif">
			<?php foreach( $v->categories as $cat_array ) : ?>
				<a href="<?php echo $cat_array->slug; ?>"><?php echo $cat_array->name; ?></a>
			<?php endforeach; ?>
		</span> 
	<?php endif; ?>

</h3>
<h2>
	<?php echo $v->post_title; ?>
</h2>
<hr>
<?php get_template_part( 'templates/block', 'video-gallery' ); ?>
