<?php

	$languages = get_product_languages(); 
	$choice_prefix = "";
	if( $toggle_language = get_field('toggle_language') ) {
		$show_popup = 1;
		// by now the cookie is set and we only need to check for it and not the GET
		if( array_key_exists( 'language_choice', $_COOKIE) ){
			$language_cookie = $_COOKIE['language_choice'];
			// double check that the language requested is still supported
			if( array_key_exists( $language_cookie, $languages ) ){
				$choice_prefix = $language_cookie;
				$language = $languages[$language_cookie];
				$show_popup = 0;
			}
		}
		if( count($_COOKIE) < 1 ){
			$show_popup = 0; //firefox is not sending cookies to server - this catches that;
		}
		if( array_key_exists('lang', $_GET ) ){
			$language_key = $_GET['lang'];
			if( array_key_exists( $language_key, $languages ) ){
				$choice_prefix = $language_key;
				$language = $languages[$language_key];
				$show_popup = 0;
			}		
		}
		elseif( count($_COOKIE) < 1 ){
	$show_popup = 1;
		}
	}
	// fallback - not necessary at the time of writing since a popup will always show if there is no language set. One MUST choose a language.
	if( !$choice_prefix ){
		$choice_prefix = "en";
		$language = "English";
	}
?>
<?php while(have_rows("product_header")): the_row(); ?>
	<?php if(get_row_layout() == "banner_video_block"): ?>
		<?php get_template_part( 'templates/block', 'banner-image-video' ); ?>
	<?php endif; ?>
	<?php if(get_row_layout() == "banner_image_block"): ?>
		<?php get_template_part( 'templates/block', 'banner-image-video' ); ?>
	<?php endif; ?>
<?php endwhile; ?>
<div class="container product-single">
	<div class="row">
		<div class="col-md-9">					
			<main class="main" role="main">

				<article <?php post_class(); ?>>
					<div class="entry-summary"></div>
					<?php echo get_field('description_'.$choice_prefix); ?>
				</article>

			</main>
		</div>
		<div class="col-md-3">					
			<aside class="sidebar" role="complementary">
				<div class="buy-now">
					<?php 
						if( get_field('sidebar_button_text') ){
							$btn_text = get_field('sidebar_button_text');
						}
						else{
							$btn_text = "Buy Now";
						}
					?>

					<a href="<?php echo get_field('infusionsoft_link',$post->ID); ?>" class="nmp-btn" target="_blank"><?php echo $btn_text; ?></a>
					<?php echo get_field( 'sidebar_content'); ?>

					<?php if( $toggle_language = get_field('toggle_language') ): ?>

						<a href="" class="nmp-btn" id="change-language">Change Language</a>

					<?php endif; ?>

				</div>
				<ul class="testimonial-slider">
					<?php 
						$testimonials = get_posts(array(
							'post_type' => 'testimonial',
							'meta_query' => array(
								array(
									'key' => 'products', // name of custom field
									'value' => '"' . $post->ID . '"',
									'compare' => 'LIKE'
								)
							)
						));
						foreach( $testimonials as $testimonial ) :  $t_id = $testimonial->ID; ?>
						<?php 
							$t_image_array = get_field('product_testimonial_image', $t_id );
							$t_content = get_field('testimonial', $t_id );
						?>
						<li>
							<p><?php echo $t_content; ?></p>
							<div class="attr">
								<?php optimal_image( array( 'image' => $t_image_array )); ?>
								<label name="name"><?php echo $testimonial->post_title; ?></label>
								<label name="title"><?php echo get_field('product_testimonial_tagline', $t_id ); ?></label>
							</div>
							<a class="prev-testimonial"></a>
							<a class="next-testimonial"></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</aside>
		</div>	
	</div>
</div>
<?php get_template_part( 'templates/main-content', 'blocks' ); ?>
<?php 
	$lang_content = array();
	$js_languages = array();
	foreach($languages as $prefix => $lang ){
		$content = array();
		$js_languages[] = array('prefix'=>$prefix, 'name'=>$lang);
		if( $description = get_field('description_'.$prefix)){
			$content['description'] = $description;
		}
		if( $excerpt = get_field('excerpt'.$prefix)){
			$content['excerpt'] = $excerpt;
		}
		$lang_content[$prefix] = $content;
	}
?>
<?php if( $toggle_language ) : ?>

	<script>//<![CDATA[
		
		var show_popup = <?php echo $show_popup; ?>;
	
		var js_languages = <?php echo json_encode( $js_languages ); ?>;
	//]]></script>

<?php endif; ?>