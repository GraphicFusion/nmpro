<div class="container">
	<h1 class="page-title">Products</h1>
	<div class="row products-list">
		<?php while (have_posts()) : the_post(); ?>
		  <?php get_template_part('templates/content', 'product'); ?>
		<?php endwhile; ?>	
	</div>
</div>