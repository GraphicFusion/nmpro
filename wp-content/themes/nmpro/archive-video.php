<?php $videos_page = get_page_by_title('videos'); ?>
<div class="row cblock video-gallery-block">
	<div class="container">
		<div class="row">
			<?php while(have_rows("video_archive_custom_content", $videos_page->ID)): the_row(); ?>
				<?php if(get_row_layout() == "video_block"):
						$video_id = get_sub_field('video');
						$video = get_post( $video_id );
						$v = get_video_meta( $video );  // see lib/custom-videos.php
				?>
					<?php get_template_part('templates/content', 'single-video'); ?>
				<?php endif; ?>

			<?php endwhile; ?>
		</div>

		<div class="row">
			<div class="col-md-6">
				<h3 class="videos-page-subtitle">Exclusive Videos</h3>
			</div>
			<div class="col-md-6">
			</div>			
		</div>

		<div class="row videos-list">
			<?php while (have_posts()) : the_post(); 
				 get_template_part('templates/content', 'video-list-item');
			endwhile; ?>	
		</div>

	</div>
</div>



				