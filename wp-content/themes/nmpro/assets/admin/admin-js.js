if(typeof jQuery!="undefined"){

	var $ = jQuery;
	if( $('.acf-field-dorado-add-sidebar input').is(':checked') ){
		$('.acf-field-use-custom-sidebar').show();
	}
	$(document).ready(function() { 

		menus = ['#menu-comments','#menu-media','#toplevel_page_gf_edit_forms','#menu-appearance','#menu-tools','#menu-settings', '#menu-plugins','#menu-posts','#menu-users', '#toplevel_page_edit-post_type-acf-field-group', '#menu-posts-testimonial'];
		function hide_all(){
			$.each( menus, function(m,v){
				$(v).hide();
			});
		}
		function show_all(){
			$.each( menus, function(m,v){
				$(v).show();
			});
		}
		hide_all();
		$('li.toplevel_page_view_more').on("click", function(e){
			e.preventDefault();
			if( !$('#menu-comments').is(':visible') ){
				show_all();
			}
			else{
				hide_all();
			}
		});

		$('.acf-flexible-content .layout').addClass('closed');
		$('.acf-field-dorado-add-sidebar input').on('change', function(){
			if($(this).is(':checked') ){
				$('.acf-field-use-custom-sidebar').show();
			}
			else{
				$('.acf-field-use-custom-sidebar').hide(); // and uncheck
				$('.acf-field-use-custom-sidebar input').attr('checked', false); 
			}
		});
	}); 
}