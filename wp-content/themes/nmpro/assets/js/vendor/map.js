;jQuery(document).ready(function($) {
	if( $('#map-canvas').length){
		function initialize() {

			mapLng -= .05;
			var mapCenter = new google.maps.LatLng(mapLat,mapLng);
			var mapOptions = {
				zoom: 12,
				center: mapCenter,
//				mapTypeId: google.maps.MapTypeId.HYBRID,
				overviewMapControl: false,
				panControl: true,
				draggable: false,
				zoomControl: true,
				scrollwheel: false,
				disableDefaultUI: true,
			}
		
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
		    var infoWindow = new google.maps.InfoWindow(), marker, i;
	
			// build a global array for accessing all the markers globally
	  		allMarkers = [];
	
			for( i = 0; i < markers.length; i++ ) {
		        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
	
		        marker = new google.maps.Marker({
		            position: position,
		            map: map,
		            title: markers[i][0],
		            link: markers[i][4],
		        });
		        marker.set("type", markers[i][3]);
				allMarkers.push(marker);
	
		        // Allow each marker to have an info window    
		        google.maps.event.addListener(marker, 'click', (function( marker, i) {
			        return function() {
			            /* setup ajax loader for marker descriptions */
						var $content = "<a href='" + marker.link + "'>" + marker.title + "</a>";
		                infoWindow.setContent( $content );
		                infoWindow.open(map, marker);
		            }
		        })(marker, i));	
	
		    }
	
			// STYLE MAP		
			var styles = [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2e5d4"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#f3ede3"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#bfb9aa"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#97c8c6"}]}];
			map.setOptions({styles: styles});
		}
			
		google.maps.event.addDomListener(window, 'load', initialize);
	
	}
});