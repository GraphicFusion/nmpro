/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can 
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {
  

// Use this variable to set up the common and page specific functions. If you 
// rename this variable, you will also need to rename the namespace below.
var Roots = {

 // All pages
  common: {
    init: function() {

      // Sliding menu
      var padding= $('.stickyfooter').css('padding');

      $('button.navbar-toggle').click(function () {

			if( $('#wrapper').hasClass('active') ){
				$('#wrapper').removeClass('active');
				//setTimeout( function(){
					$('#wrapper').addClass('inactive');
				//}, 500 );
			}
			else{
				$('#wrapper').removeClass('inactive');
				//setTimeout( function(){
					$('#wrapper').addClass('active');
				//}, 200 );
			}
          $(this).toggleClass('active');

          if ($('#wrapper').hasClass('active')) {

            $('header').css('left','0px');
            $('header button.navbar-toggle').css({'left':'30px', 'right':'initial'});
            //$('.navbar-brand').addClass('hide');

            // Compensate for our stickyfooter being a bitch
            $('#page-footer').addClass('hide');
            $('.action-bar.bottom').addClass('hide');
            $('.stickyfooter').css({'height':'100%'});
            $('.st-menu').removeClass('invisible');
            $('#header-logo').hide();

            // Hide elements and display only the one that is toggled
            $('button.navbar-toggle').addClass('hide');
            $(this).removeClass('hide');
//            $('#sidebar-wrapper .mobile-menu').addClass('hide');
            $('#sidebar-wrapper').show();
			//$('.st-pusher').height( $('.mobile-menu').height() + 150 ); // 150 plus is hack 
          } else {

            $('header').css('left','0px');
            $('header button.navbar-toggle').css({'right':'10px', 'left':'initial'});
            $('header button.navbar-toggle.toggle-two').css({'right':'58px', 'left':'initial'});

            $('.navbar-brand').removeClass('hide');
//            $('#sidebar-wrapper').hide();

            // Turn our sticky footer back on
            $('#page-footer').removeClass('hide')
            $('.action-bar.bottom').removeClass('hide');
            $('.stickyfooter').css('padding',padding);
            $('#header-logo').show();

            // Hide menu and show toggles
            $('button.navbar-toggle').removeClass('hide');

//			$('.st-pusher').height( '');


            window.setTimeout(function() {   //need this timeout otherwise it shows up weird
                $('.stickyfooter').css('height','auto');
            },300);
          }

        });

        // mobile dropdown menu
        $('.navbar-mobile .dropdown:not(.on) .dropdown-toggle').click(function (event) {
            if ( ! $(this).parent().hasClass('on') ) {
                event.preventDefault();
                $(this).parent().toggleClass('on');
            }
        });

        $(window).resize(function() {
          if ( $(window).width() >= 769 ) {
            $('#wrapper').removeClass('active');
          }
        });
    },
    finalize: function() { }
  },
  // Home page
  home: {
    init: function() {
      // JavaScript to be fired on the home page
    }
  },
  // About us page, note the change from about-us to about_us.
  about_us: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

$(document).ready(UTIL.loadEvents);

$(document).ready(function() { // USED FOR MOTULITPLE THINGS

    //-----| Smooth Scroll to anchors
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    //--------| Animate our header on scroll
    $(window).scroll(function() {
            
        if ($(this).scrollTop() > $('#themeslug-navbar').height() && !$('#sticky-action-bar').hasClass("sticky") ){
            $('#sticky-action-bar').addClass("sticky");
        }
        if ($(this).scrollTop() <= $('#themeslug-navbar').height() && $('#sticky-action-bar').hasClass("sticky") ){
            $('#sticky-action-bar').removeClass("sticky");
        }
        
    });
	getColorbox();	
	function getColorbox(){ 
		var viewX = Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
			video_params = {
			rel: 'gal',
	        maxWidth: '96%',
	        maxHeight: '90%',
	        fixed: true,
	        iframe:true,
	        innerWidth:640, 
	        innerHeight:390
	    };
	    var colorbox_params = {
			rel: 'gal',
	        maxWidth: '96%',
	        maxHeight: '90%',
	        fixed: true
	    };
		if( viewX > 768 ){
			$(".youtube-video").colorbox({iframe:true, innerWidth:640, innerHeight:390});
		}
	    $('.colorbox-video').colorbox(video_params);
		$('.colorbox').colorbox(video_params);
		$('a[href$=\".jpg\"], a[href$=\".png\"]').colorbox(colorbox_params);
	}
	$('.products-gallery > .product-slider').slick({
		slidesToShow: 4,
  		slidesToScroll: 1,
  		infinite: true,
		 responsive: [
			{
			  breakpoint: 992,
			  settings: {
				slidesToShow: 2
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 1
			  }
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		  ]				
	});
	$('.image-gallery-block > .gallery-slider').slick({
		infinite:true,
		slidesToShow: 100,
  		slidesToScroll: 1,
  		variableWidth:true
	});


	$('.testimonial-slider-block .testimonial-slider').slick({
		slidesToShow: 2,
  		slidesToScroll: 2,
  		nextArrow:'.next-testimonial',
  		responsive: [
			{
			  breakpoint: 992,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}]
	});
	$('aside .testimonial-slider').slick({
		slidesToShow: 1,
  		slidesToScroll: 1,
  		prevArrow:'.prev-testimonial',
  		nextArrow:'.next-testimonial'
	});

	
	$(".accordion-set > a").on("click", function(e){
		e.preventDefault();
	
		if($(this).hasClass('active')){
			$(this).removeClass("active");
			$(this).siblings('.accordion-content').slideUp(200);
			//$(".accordion-set > a i").removeClass("fa-minus").addClass("fa-plus");
		}else{
			//$(".accordion-set > a i").removeClass("fa-minus").addClass("fa-plus");
			//$(this).find("i").removeClass("fa-plus").addClass("fa-minus");
			$(".accordion-set > a").removeClass("active");
			$(this).addClass("active");
			$('.accordion-content').slideUp(200);
			$(this).siblings('.accordion-content').slideDown(200);
		}
    });
    
    
    $(".read-more-toggle").click(function(){
    	$(this).siblings(".read-more-hidden").slideToggle(300);
    	$(this).toggleClass("read-less");
    	$(this).text( $(this).text() == "Read More" ? "Read Less" : "Read More" );
    });

    $(".event-agenda .nav a").click(function(e){
    	e.preventDefault();
    	var newday = $(this).closest("li").index() + 1;
		$(this).closest("li").siblings(".active").removeClass("active");
    	
    	if($(this).closest(".event-agenda").find(".agenda-day.active")){
			$(this).closest(".event-agenda").find(".agenda-day.active").fadeOut(300, function(){
				$(this).closest(".event-agenda").find(".agenda-day.active").removeClass("active");
				$(this).closest(".event-agenda").find(".agenda-day.day-"+newday).fadeIn(300, function(){
					$(this).closest(".event-agenda").find(".agenda-day.day-"+newday).addClass("active");
				});			
				$(this).closest(".event-agenda").find(".nav li.day-"+newday).addClass("active");
			});
		}else{
			$(this).closest(".event-agenda").find(".agenda-day.day-"+newday).fadeIn(300, function(){
				$(this).closest(".event-agenda").find(".agenda-day.day-"+newday).addClass('active');
			});	
			$(this).closest(".event-agenda").find(".nav li.day-"+newday).addClass("active");
		}		
    });
});
	function deleteCookie( name ) {
		document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}
	function createCookie(name, value, days) {
	    var expires;
	
	    if (days) {
	        var date = new Date();
	        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
	        expires = "; expires=" + date.toGMTString();
	    } else {
	        expires = "";
	    }
	    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
	}
	window.setLanguage = function(el){
		var $prefix = $(el).data('prefix'),
			$href = $(el).attr('href');
		createCookie( 'language_choice', $prefix , 60 );

		window.location = $href;
	}
	if( $('.product-single').length && js_languages.length && show_popup ){		
		var $language_html = "<div id='language_choice'><h4>Select your language:</h4><ul>";
		for( var i = 0; i < js_languages.length; i++){
			if(js_languages[i].prefix=="fr"){
				$language_html += "<li><a href='http://www.networkmarketingpro.fr/' target='_blank'>" + js_languages[i].name + "</a></li>";
			}else{
				$language_html += "<li><a onClick='event.preventDefault(); window.setLanguage(this);' class='language_select'  href='" + window.location.pathname + "?lang=" + js_languages[i].prefix + "' data-prefix='"+ js_languages[i].prefix + "'>" + js_languages[i].name + "</a></li>";
			}
		}
		$language_html += "</ul>";
		$.colorbox({html:$language_html,  innerWidth:"100%", innerHeight:"100%"});
		$(document).bind('cbox_complete', function(){
			$('#colorbox, #cboxOverlay').addClass('language_popup');
		});
	}
		$('#change-language').on('click', function(e){
			e.preventDefault();
			var $path = window.location.pathname;
			document.cookie = 'language_choice=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			window.location.href = $path;
		});

		jQuery('.live-video .video > iframe[width][height]').each(function(){
			var vidwidth = jQuery(this).attr('width');
			var vidheight = jQuery(this).attr('height');
			
			if(!isNaN(vidheight) && !isNaN(vidwidth) ){
				aspectratio = vidheight/vidwidth * 100;
				jQuery(this).removeAttr('width').removeAttr('height');
				jQuery(this).parent().css({'width':'100%','max-width':vidwidth,'height':'0','padding-bottom':aspectratio+'%'});
				jQuery(this).css({'width':'100%','height':'100%','position':'absolute','left':'0','top':'0'});
			}

		});


})(jQuery); // Fully reference jQuery after this point.
