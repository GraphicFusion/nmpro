;jQuery(document).ready(function($) {
	if( $('#map-canvas').length){	
		function initialize() {

			var mapCenter = new google.maps.LatLng(mapLat,mapLng);
			var mapOptions = {
				zoom: 10,
				center: mapCenter,
//				mapTypeId: google.maps.MapTypeId.HYBRID,
				overviewMapControl: false,
				panControl: true,
				draggable: false,
				zoomControl: true,
				scrollwheel: false,
				disableDefaultUI: true,
			}
		
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
		    var infoWindow = new google.maps.InfoWindow(), marker, i;
	
			// build a global array for accessing all the markers globally
	  		allMarkers = [];
	
			for( i = 0; i < markers.length; i++ ) {
		        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
	
		        marker = new google.maps.Marker({
		            position: position,
		            map: map,
		            title: markers[i][0],
		            link: markers[i][4],
		        });
		        marker.set("type", markers[i][3]);
				allMarkers.push(marker);
	
		        // Allow each marker to have an info window    
		        google.maps.event.addListener(marker, 'click', (function( marker, i) {
			        return function() {
			            /* setup ajax loader for marker descriptions */
						var $content = "<a href='" + marker.link + "'>" + marker.title + "</a>";
		                infoWindow.setContent( $content );
		                infoWindow.open(map, marker);
		            }
		        })(marker, i));	
	
		    }
	
			// STYLE MAP		
			var styles = [{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#c0e8e8"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7dcdcd"}]}];			map.setOptions({styles: styles});
		}
			
		google.maps.event.addDomListener(window, 'load', initialize);
	
	}
});