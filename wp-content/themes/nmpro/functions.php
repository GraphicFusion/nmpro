<?php
/**
 * Sage includes
 *
 * The $dorado_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$dorado_includes = array(
	'lib/utils.php',            			     			// Utility functions
	'lib/init.php',                  						// Initial theme setup and constants
	'lib/nav.php',                  						// Nav Walker
	'lib/wrapper.php',               						// Theme wrapper class
	'lib/conditional-tag-check.php', 						// ConditionalTagCheck class
	'lib/config.php',                						// Configuration
	'lib/enqueue.php',                						// Scripts and stylesheets
	'lib/extras.php',                						// Custom functions
	'lib/dorado_header_hook.php',  							// Header Hook
	'lib/menus.php',  										// Admin Menu functions
	'lib/admin.php',  										// Admin functions
	'lib/custom-products.php',								// ACF Based Custom Products
	'lib/acf-site-settings.php',							// ACF Sitewide Field Definitions and Functions (sidebar etc)
	'lib/custom-events.php',								// ACF Based Custom Events
	'lib/custom-videos.php',								// ACF Based Custom Videos - for managing YouTube Links
	'lib/custom-locations.php',								// ACF Based Custom Locations
	'lib/custom-speakers.php',								// ACF Based Custom Speakers
	'lib/custom-testimonials.php',							// ACF Based Custom Testimonials
	'lib/is-vendor/autoload.php',							// Infusionsoft 
	'lib/dorado_functions.php',								// General Theme Specific Functions 
	'lib/is-vendor/autoload.php'
// add extra includes here
);


foreach ($dorado_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'dorado'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

use Sonder\Dorado\Config;   
use Sonder\Dorado\Wrapper;

// ADMIN STYLES
add_action( 'admin_enqueue_scripts', 'dorado_admin_enqueue' );
function dorado_admin_enqueue(){
	wp_register_script( 'dorado_admin_js', get_template_directory_uri() . '/assets/admin/admin-js.js', array('jquery') , null, true );
	wp_enqueue_script('dorado_admin_js');
	wp_register_style( 'dorado_admin_css', get_template_directory_uri() . '/assets/admin/admin-style.css', false, '1.0.0' );
	wp_enqueue_style( 'dorado_admin_css' );
}
function register_wp_session(){
	if( !session_id() ){
		session_start();
	}
}
add_action('init', 'register_wp_session');
function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'){
	$str = '';
	$max = mb_strlen($keyspace, '8bit') - 1;
	for ($i = 0; $i < $length; ++$i) {
	    $str .= $keyspace[rand(0, $max)];
	}
	return $str;
}
