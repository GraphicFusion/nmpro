<?php
	if( get_sub_field('number_of_events') ){
		$number = get_sub_field('number_of_events');
	}
	if( !isset($number) ){
		$number = 3;
	}
	$eventObj = new doradoEvents;
	$eventObj->get_events('','','', $number );
	$events_by_stamp = $eventObj->events;
	ksort($events_by_stamp);
	$events = array();
	
	// get featured event
	$events_page_obj = get_page_by_title('events');
	$featured_event = get_field('featured_event', $events_page_obj->ID);
	$featured_id = "";
	if( is_object($featured_event)){
		$featured_id = $featured_event->ID;
	}
	$events[] = $featured_event;
	foreach( $events_by_stamp as $stamp => $events_array ){			
		foreach( $events_array as $event ){
			if( $event['post'] != $featured_id ){
				$e_post = get_post( $event['post'] );
				$e_post->location = get_field('event_location_text', $e_post->ID);	
				$events[] = $e_post;
			}
		}
	} 
?>
<div class="container">
	<h1 class="page-title">Events</h1>
	<?php if(count($events) > 0 ) : ?>
	
		<div class="row events-list">
		<?php $i=0;
		foreach( $events as $event ) :
			if (is_object( $event )) :
				$start_timestamp = get_field("start_timestamp",$event->ID);
				$start_month = date( 'M', $start_timestamp);
				$start_day = date( 'j', $start_timestamp);
				$start_year = date( 'Y', $start_timestamp);
	
				$end_timestamp = get_field("end_timestamp",$event->ID);
				$end_month = date( 'M', $end_timestamp);
				$end_day = date( 'j', $end_timestamp);
				$end_year = date( 'Y', $end_timestamp);
				
				$event_excerpt = get_field("landing_excerpt",$event->ID); ?>
	
				<div class="col-xs-12 <?php echo ($i==0) ? 'col-sm-12' : 'col-sm-6';?>">
					<a href="<?php echo get_permalink($event->ID); ?>" style="background-image: linear-gradient(rgba(22,34,46,0.8),rgba(22,34,46,0.8)), url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($event->ID) ); ?>')">
						<div class="event">
							<h2><?php echo $event->post_title; ?></h2>
							<hr />
					
							<?php if ($start_year==$end_year){
									if ($start_month==$end_month){
										if ($start_day==$end_day)
											echo '<h3>'.$end_month.' '.ordinal($end_day).', '.$end_year.'</h3>';
										else
											echo '<h3>'.$end_month.' '.ordinal($start_day).' &ndash; '.ordinal($end_day).', '.$end_year.'</h3>';
									}else
										echo '<h3>'.$start_month.' '.ordinal($start_day).' &ndash; '.$end_month.' '.ordinal($end_day).', '.$end_year.'</h3>';
								}
								else
									echo '<h3>'.$start_month.' '.ordinal($start_day).', '.$start_year.' &ndash; '.$end_month.' '.ordinal($end_day).', '.$end_year.'</h3>';
							?>
						
							<h4><?php echo $event->location; ?></h4>
						</div>
					</a>
				</div>
			<?php
				$i++;
				endif;					
				endforeach; 
			?>	
		<?php endif; ?>
	</div>
</div>
