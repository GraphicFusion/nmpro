<div class="side-search">
    <?php get_template_part('templates/searchform') ?>
</div>

<?php if (has_nav_menu('mobile-menu')) :  //-----| Mobile menu ?>

    <div class="mobilemenu-two mobile-menu">
        <?php wp_nav_menu(array('theme_location' => 'mobile-menu', 'menu_class' => 'nav navbar-nav secmobile')); ?>
    </div>

<?php else: ?>

    <?php if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
    endif; ?>

<?php endif; ?> 