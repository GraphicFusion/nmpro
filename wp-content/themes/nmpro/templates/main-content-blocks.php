<?php while(have_rows("custom_content")): the_row(); ?>

	<?php if(get_row_layout() == "video_block"): ?>

		<?php get_template_part( 'templates/block', 'video' ); ?>

	<?php elseif(get_row_layout() == "banner_video_block"): ?>

		<?php get_template_part( 'templates/block', 'banner-image-video' ); ?>

	<?php elseif(get_row_layout() == "banner_image_block"): ?>

		<?php get_template_part( 'templates/block', 'banner-image-video' ); ?>

	<?php elseif(get_row_layout() == "events_block"): ?>

		<?php get_template_part( 'templates/block', 'event-gallery' ); ?>

	<?php elseif(get_row_layout() == "videos_block"): ?>

		<?php get_template_part( 'templates/block', 'video-gallery' ); ?>

	<?php elseif(get_row_layout() == "products_block"): ?>

		<?php get_template_part( 'templates/block', 'product-gallery' ); ?>

	<?php elseif(get_row_layout() == "testimonials_block"): ?>

		<?php get_template_part( 'templates/block', 'testimonial-slider' ); ?>

	<?php elseif(get_row_layout() == "floating_block"): ?>

		<?php get_template_part( 'templates/block', 'floating-text' ); ?>

	<?php elseif(get_row_layout() == "read_more_block"): ?>

		<?php get_template_part( 'templates/block', 'read-more' ); ?>

	<?php elseif(get_row_layout() == "learn_more_block"): ?>

		<?php get_template_part( 'templates/block', 'learn-more' ); ?>

	<?php elseif(get_row_layout() == "image_gallery_block"): ?>

		<?php get_template_part( 'templates/block', 'image-gallery' ); ?>

	<?php elseif(get_row_layout() == "buy_now_block" && !is_singular('product')): ?>

		<?php get_template_part( 'templates/block', 'product-footer' ); ?>

	<?php endif; ?>

<?php endwhile; ?>