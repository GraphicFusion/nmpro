<?php
	$videos = get_sub_field('video_testimonial_repeater');
	if( is_array( $videos ) ) : ?>
?>
	<div class="row video-testimonials cblock">
		<a class="anchor" name="video-testimonials"></a>
		<div class="container">
			<h1>Video Testimonials</h1>
			<?php foreach( $videos as $video ): 
					$v = get_video_meta( $video ); // see lib/custom-videos.php
			?>
				<div class="col-md-4">
					<a class="video-thumb youtube-video" href="https://www.youtube.com/embed/<?php echo $v->youtube; ?>?rel=0&autohide=2&modestbranding=1&autoplay=1" style="background-image:url(<?php echo $v->background_image; ?>);"></a>
					<h2><?php echo $v->testimonial_name; ?></h2>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>