<?php
	$gallery = get_sub_field('images');
?>

<div class="row cblock image-gallery-block no-gutter">
	<div class="gallery-slider">
		<?php if(is_array($gallery)) :?>
			<?php for($g=0;$g<4;$g++): // temp fix to fill out page
				foreach( $gallery as $image_array ) : 
					$extension_pos = strrpos($image_array['url'], '.'); // find position of the last dot, so where the extension starts
					$thumb = substr($image_array['url'], 0, $extension_pos) . '-150x150' . substr($image_array['url'], $extension_pos);
					?>
					<a class="slide" href="<?php echo $image_array['url']; ?>" class="cboxElement" style="background-image:url(<?php echo $thumb; ?>)"></a>
				<?php endforeach; ?>
			<?php endfor; ?>
		<?php endif; ?>
	</div>
</div>
