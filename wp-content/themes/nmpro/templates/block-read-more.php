<div class="row read-more-block cblock">
	<a class="anchor" name="read-more-block"></a>
	<div class="container">
		<div class="read-more-main col-md-12 col-lg-8 col-lg-offset-2">
			<?php echo get_sub_field('read_more_main',$post->ID); ?>
		</div>
		<?php if( $more = get_sub_field('read_more_hidden',$post->ID) ):  ?>
			<div class="read-more-hidden col-md-12 col-lg-8 col-lg-offset-2">
				<?php echo $more; ?>
			</div>
			<h4 class="read-more-toggle">Read More</h4>
		<?php endif; ?>
	</div>
</div>