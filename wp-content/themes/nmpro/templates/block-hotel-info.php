<?php global $event; 
	$event_locations = get_field('event_locations', $event->ID );
	foreach( $event_locations as $key=>$location_array ){
		$location = $location_array['event_location'];
		$location_meta = get_field('loc_location', $location->ID );
		foreach($location_meta as $key=>$val){
			$location->$key = $val;
		}
	}
	if( is_object( $location ) ): 
		$location->custom_address = get_field('custom_address', $location->ID);	
		$location->loc_phone_number = get_field('loc_phone_number', $location->ID);	
?>
<div class="row hotel-info-block cblock">
	<a class="anchor" name="hotel-info-block"></a>
	<div class="container">
		<h1>Hotel Information</h1>
		<div class="col-md-12 col-lg-10 col-lg-offset-1">
			<ul>
			<?php if( have_rows('pdfs', $location->ID) ): ?>
				<?php while ( have_rows('pdfs', $location->ID) ) : the_row(); 
						$file_array = get_sub_field('loc_file'); ?>  
					<li><a target="_blank" href="<?php echo $file_array['url']; ?>">Download <?php echo $file_array['title']; ?></a></li>
				<?php endwhile; ?>		
			<?php endif; ?>			
		</div>
	</div>
</div>
<?php endif; ?>