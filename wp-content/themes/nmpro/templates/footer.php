<?php if(!is_singular('product')): ?>

	<div class="row action-bar bottom no-gutter">
		<?php get_template_part( 'templates/block', 'footer-action-block' ); ?>
	</div> 

<?php else: ?>
	<?php while(have_rows("custom_content")): the_row(); ?>
		<?php if(get_row_layout() == "buy_now_block" ): 
				if ( $banner = get_sub_field('buy_now_background_image',$post->ID) ){
				    $args = array( 
				        'image'     => $banner,
				        'background'    => true,
				    ); 
				}
		?>

		<div class="row action-bar bottom product-footer" <?php if(is_array( $args) ){ optimal_image( $args ); } ?>>
			<?php get_template_part( 'templates/block', 'product-footer' ); ?>
		</div> 
		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>
<footer class="content-info" role="contentinfo" id="page-footer">
  <div class="container">

			<ul><?php echo return_social_items(); ?></ul>
			<nav class="footer-nav" role="navigation">
				<?php
					if (has_nav_menu('footer-menu')) {
						wp_nav_menu(array('theme_location' => 'footer-menu', 'menu_class' => 'nav navbar-nav'));
					};
				?>
			</nav>
			<p class="copyright">Copyright &copy; <?php echo date('Y'); ?> Network Marketing Pro, Inc. All Rights Reserved.</p>
  </div>
</footer>
