<?php
	if( get_sub_field('automatic_products') ){
		$number = get_sub_field('number_of_products');
		if( !$number ){
			$number = 1;
		}
		$args = array('post_type' => 'product','posts_per_page'=>$number);
		$result = new WP_Query($args);
		if($result->have_posts()){
			$products = $result->posts;
		}				
	}
	else{
		$repeater = get_sub_field('products_repeater');
		foreach( $repeater as $row ) { 
			$products[] = $row['product'];
		}
	}
?>

<div class="row products-block cblock">
	<div class="container-xl gallery products-gallery">
		<div class="product-slider">
			<?php if(is_array($products)) :?>
				<?php foreach( $products as $product ) : ?>

					<div class="gallery-item col-xs-12 col-sm-6 col-md-3" <?php if($thumbnail_id = get_post_thumbnail_id($product->ID)) echo 'style="background-image: url('. wp_get_attachment_url( $thumbnail_id ) .')"'; ?>>
						<div class="bottom-stick">
							<?php if( $product->post_name == 'go-pro-all-access'): ?>
								<a class="green-btn nmp-btn" target="_blank" href="https://goproallaccess.com/">
									<?php echo $product->post_title; ?>
								</a>
							<?php else: ?>
								<a class="green-btn nmp-btn" href="<?php echo get_permalink( $product->ID ); ?>">
									<?php echo $product->post_title; ?>
								</a>
							<?php endif; ?>							
							<div class="item-description">
								<?php 
									$item_desc = get_field('excerpt_en', $product->ID);
									$stop_pts = array();
									$punc_array = array(' ', '.', ',', '!', ':', ';');
									if( strlen($item_desc) > 180 ){
	
										foreach($punc_array as $needle) {
											if(($pos = strpos($item_desc, $needle, 180))!==false) $stop_pts[] = $pos;
										}
										echo substr($item_desc, 0, min($stop_pts)) . '...';
									}
									else{
										echo $item_desc; 
									}
								?>
							</div>
						</div>
					</div>

				<?php endforeach; ?>
			<?php endif; ?>
			
					<div class="gallery-item col-xs-12 col-sm-6 col-md-3" style="background-image: url(/wp-content/uploads/2015/10/vegas_strip.jpg)">
						<div class="bottom-stick">
							<a class="green-btn nmp-btn" href="/events">View Events</a>
							<div class="item-description">Discover MLM training from Eric Worre and million dollar earning mentors.</div>
						</div>
					</div>

		</div>
	</div>
</div>