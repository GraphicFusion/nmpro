<?php
	$video_id = get_sub_field('video');

	$prefix = "bi"; // acf field key prefix; distinguishes between image group and video group; - banner image (bi) vs banner video (bv) 
	if( $video_id ){
		$prefix = "bv";
		$video = get_post( $video_id );
		$cat = get_sub_field( 'video_category');
		$overlayColor = get_sub_field('bv_container_color');
		$opacity = get_sub_field('bv_container_opacity');
		$video->category = $cat->name;
		$video->cat_slug = $cat->slug;
		$banner = get_video_meta( $video ); // see lib/custom-videos.php
		if( $bg_image = get_sub_field( 'bv_background_image') ){
			$banner->background_image = $bg_image['url'];
		}
	}
	else{
		$banner = new stdClass();
		$background_image = get_sub_field('bi_background_image');
		$banner->background_image = $background_image['url'];
		$overlayColor = get_sub_field('bi_container_color');
		$opacity = get_sub_field('bi_container_opacity');
	}
	if( get_sub_field($prefix.'_title') ){
		$banner->title = get_sub_field($prefix.'_title');
	}
	$banner->subtitle = get_sub_field($prefix.'_subtitle');
	$more_url = "/videos";
	if( get_field('toggle_product_title') ){
		unset( $banner->subtitle);
		$banner->title = "";
	}
	
	if($overlayColor){
		$rgb = implode ( ',' , getrgb( $overlayColor) );		
		$overlay = 'linear-gradient(rgba('. $rgb .','. ( $opacity/100 ).'),rgba('. $rgb .','.( $opacity/100 ).'))';
		$banner_background_image = $overlay.', url('.$banner->background_image.')';
	}
	else
		$banner_background_image = 'url('.$banner->background_image.')';

if(is_front_page()): ?>
<div class="row cblock video-block" style="background-image:<?php echo $banner_background_image; ?>">
	<div class="container">
		<div class="video-info">
			<?php if( property_exists( $banner, 'youtube' ) && $banner->youtube ) : ?>
				<a class="<?php echo $banner->a_class; ?> video-title" href="https://www.youtube.com/embed/<?php echo $banner->youtube; ?>?rel=0&autohide=2&modestbranding=1&autoplay=1">
					<h1><?php echo $banner->title; ?></h1>
				</a>
			<?php elseif( property_exists( $banner, 'vimeo' ) && $banner->vimeo ) : ?>
			<?php else : ?>
				<h1><?php echo $banner->title; ?></h1>
			<?php endif; ?>
			<hr />
			<h4>
				<span class="serif">Site Videos</span> | <a href="<?php echo $more_url; ?>">View More</a>
			</h4>
		</div>
	</div>
</div>
<?php else: ?>
<div class="page-header-block row" style="background-image:<?php echo $banner_background_image; ?>">
	<div class="container">
		<div class="col-md-6 col-md-offset-3">
			<?php if( property_exists( $banner, 'youtube' ) && $banner->youtube ) : ?>
				<h1>
					<a class="<?php echo $banner->a_class; ?>" href="https://www.youtube.com/embed/<?php echo $banner->youtube; ?>?rel=0&autohide=2&modestbranding=1&autoplay=1"><?php echo $banner->title; ?></a>
				</h1>
				<?php if( property_exists( $banner, 'subtitle' ) && $banner->subtitle) : ?>
					<h3>
						<a><?php echo $banner->subtitle; ?></a>
					</h3>
				<?php endif; ?>
				<a class="<?php echo $banner->a_class; ?> video-button" href="https://www.youtube.com/embed/<?php echo $banner->youtube; ?>?rel=0&autohide=2&modestbranding=1&autoplay=1"></a>
			<?php else: ?>
					<h1><?php echo $banner->title; ?></h1>
				<?php if( property_exists( $banner, 'subtitle' ) && $banner->subtitle) : ?>
					<h3><?php echo $banner->subtitle; ?></h3>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>