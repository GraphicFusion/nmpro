<?php 
	$v = get_video_meta($post); 

?><div class="gallery-item col-xs-12 col-sm-6 col-md-3">
	<a class="video-thumb youtube-video" href="https://www.youtube.com/embed/<?php echo $v->youtube; ?>?rel=0&autohide=2&modestbranding=1&autoplay=1" style="background-image:url(<?php echo $v->background_image; ?>);">
		<?php if( $v->durationText ) : ?>
			<div class="time"><?php echo $v->durationText; ?></div>
		<?php endif; ?>
	</a>

	<h5 class="video-cat"><a href="/video-cat/<?php echo $v->cat_slug; ?>"><?php echo $v->category; ?></a></h5>
	<p>
		<?php
			$str = substr( $v->excerpt, 0, 80);
			$result = preg_replace( "/\s[a-z:]+$/i", "", $str );
			if( $str ) : echo $str; ?> <a href=''>...</a>
		<?php endif; ?>
	</p>
</div>