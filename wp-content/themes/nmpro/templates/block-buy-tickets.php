<?php 
	if ( $banner = get_field('price_package_background_image',$post->ID) ){
	    $args = array( 
	        'image'     => $banner,
	        'background'    => true,
	    ); 
	}
?>

	<div class="row buy-tickets cblock" <?php if( is_array( @$args ) ){ optimal_image( $args ); }?>>
		<div class="container">
			<a class="anchor" name="buy-tickets"></a>
			<div class="row">
				<div class="col-md-12 col-lg-8"><?php echo get_field('package_introduction', $post->ID); ?></div>
			</div>
			<div class="row">
				<?php if( have_rows('event_price_packages') ): ?>
					<?php while ( have_rows('event_price_packages') ) : the_row(); ?>
						<div class="col-md-4">			
							<h3><?php echo the_sub_field('package_text'); ?></h3>
							<a href="<?php echo the_sub_field('package_link'); ?>" class="nmp-btn">Buy Now</a>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
