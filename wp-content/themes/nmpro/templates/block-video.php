<?php
	$video_id = get_sub_field('video');
	$video = get_post( $video_id );
	$v = get_video_meta( $video );  // see lib/custom-videos.php
	$more_url = "/videos";
?>
<div class="row cblock video-block" style="background-image:url(<?php echo $v->background_image; ?>)">
	<div class="container">
		<div class="video-info">
			<a class="youtube-video video-title" href="https://www.youtube.com/embed/<?php echo get_video_id( $v->youtube ); ?>?rel=0&autohide=2&modestbranding=1&autoplay=1">
				<h1>
					<?php echo $video->post_title; ?>
				</h1>
			</a>
			<hr />
			<h4>
				<span class="serif">Site Videos</span> | <a href="<?php echo $more_url; ?>">View More</a>
			</h4>
		</div>
	</div>
</div>