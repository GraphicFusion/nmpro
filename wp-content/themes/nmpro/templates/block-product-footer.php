<div class="container">
	<div class="col-md-6">
		<p><?php the_sub_field('buy_now_content',$post->ID); ?></p>
		<a class="nmp-btn" href="<?php echo get_field('infusionsoft_link',$post->ID); ?>" target="_blank">Buy Now</a>
	</div>
</div>
