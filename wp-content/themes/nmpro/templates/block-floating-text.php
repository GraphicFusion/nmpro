<?php 
	if ( $banner = get_sub_field('floating_background_image',$post->ID) ){
	    $args = array( 
	        'image'     => $banner,
	        'background'    => true,
	    ); 
	}
	//https://bitbucket.org/tripgrass/optimal-image
	$style = "";
	$col_classes = "col-md-7 col-md-offset-1"; // default left foat class
	if( $align = get_sub_field('floating_align',$post->ID) ){
		if( $align == 'right' ){
			$col_classes = "col-md-7 col-md-offset-4"; // class to float right
		}
		if( $align == 'center' ){
			$col_classes = "col-md-12"; // class to float right
			$style = 'style="text-align:center;"';
		}
	}
	$button = "";
	if( $text = get_sub_field('floating_button_text',$post->ID)){
		$button = '<a class="nmp-btn" href='.get_sub_field('floating_link',$post->ID).'>'.$text.'</a>';
	}
	$title = "";
	if(	$title = get_sub_field('floating_title',$post->ID) ){
		$title = "<h1>".$title."</h1>";
	}

?>
<div class="row floating-text-block cblock" <?php if( is_array( @$args ) ){ optimal_image( $args ); }?>>
	<div class="container">
		<div class=" <?php echo $col_classes; ?>" <?php echo $style; ?>>
			<?php echo $title; ?>
			<?php the_sub_field('floating_content',$post->ID); ?>
			<?php echo $button; ?>
		</div>
	</div>
</div>