<?php
	$video_id = get_sub_field('video');
	$video = get_post( $video_id );
	$v = get_video_meta( $video ); // see lib/custom-videos.php
	if( $override_video_background = get_sub_field('event_video_image', $video_id ) ){
		$v->background_image = $override_video_background;
	}
	$more_url = "/videos";

	$imageArray = get_sub_field('event_background_image', $post->ID);
	$overlayColor = get_sub_field('event_container_color', $post->ID);
	$rgba = implode ( ',' , getrgb( $overlayColor) );

	global $event; // declared at top of single-event.php
	$dates = $event->start_month." ".$event->start_day;
	if( $event->start_month == $event->end_month && $event->start_day != $event->end_day || $event->start_month != $event->end_month){
		$dates .= " - ";
		if( $event->start_month != $event->end_month){
			$dates .= " ".$event->end_month." ";
		}
		$dates .= $event->end_day;
	}

	$video_event_title = get_sub_field('event_title', $video_id );
	if( !$video_event_title ){
		$video_event_title = $event->title;
	}	
	$video_event_sub_title = get_sub_field('event_sub_title', $video_id);
?>
	<?php if(is_array( $imageArray ) ) : ?>
	<div class="row cblock event-video-block" style="background-image: linear-gradient(rgba(<?php echo $rgba; ?> , <?php echo get_sub_field('event_container_opacity')/100; ?>),rgba(<?php echo $rgba; ?> , <?php echo get_sub_field('event_container_opacity')/100; ?>)), url('<?php echo $imageArray['url']; ?>'); background-color: rgb(<?php echo $rgba; ?>);">
		<a class="anchor" name="event-video-block"></a>
		<div class="container">
			<div class="col-md-6 col-md-push-6">
				<a class="youtube-video video-thumb" href="https://www.youtube.com/embed/<?php echo $v->youtube; ?>?rel=0&autohide=2&modestbranding=1&autoplay=1" <?php 
						$args = array ( 'image' => $v->background_image, 'background' => true);
						// https://bitbucket.org/tripgrass/optimal-image
						optimal_image( $args );
					?>></a>			
			</div>
			<div class="event-title col-md-6 col-md-pull-6">
				<h1><?php echo $video_event_title; ?></h1>
				<?php if ($video_event_sub_title ) : ?>
					<h2><?php echo $video_event_sub_title; ?></h2>
				<?php endif; ?>
			</div>
			<hr class="col-md-4 col-md-offset-4">
			<h3><?php echo $dates; ?> | <?php echo $event->location; ?></h3>
		</div>
	</div>
	<?php endif; ?>