<?php $post->ID; if( $qas = get_sub_field('learn_more_repeater', $post->ID) ): ?>
<div class="row learn-more-block cblock">
	<a class="anchor" name="learn-more-block"></a>
	<div class="container">
		<h1>Learn More</h1>
		<div class="accordion-container col-md-12 col-lg-10 col-lg-offset-1">
			<?php foreach( $qas as $qa ) : ?>
		
				<div class="accordion-set">
					<a href="#"><?php echo $qa['lm_question']; ?></a>
					<div class="accordion-content">
						<p><?php echo $qa['lm_answer']; ?></p>
					</div>
				</div>
			<?php endforeach; ?>		
			
		</div>
	</div>
</div>
<?php endif; ?>