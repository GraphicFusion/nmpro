<?php 
	if(!is_singular('event'))
		get_template_part( 'templates/content', 'action-blocks' ); 
	else{
		$menu_array = array();
		while(have_rows("event_landing_content")){
			the_row(); 
			if(get_row_layout() == "events_video_block"){
				if( !in_array( "event-video-block", $menu_array ) ){ 
					//skip this menu item
					//$menu_array[]="event-video-block";
				}
			}elseif(get_row_layout() == "read_more_block"){
				if( !in_array( "read-more-block", $menu_array ) ){ 
					//skip this menu item
					//$menu_array[]="read-more-block";
				}
			}elseif(get_row_layout() == "key_speakers_block"){
				if( !in_array( "key-speakers", $menu_array ) ){ 
					$menu_array[]="key-speakers";
				}
			}elseif(get_row_layout() == "guest_speakers_block"){
				if( !in_array( "guest-speakers", $menu_array ) ){ 
					//skip this menu item
					//$menu_array[]="guest-speakers";
				}
			}elseif(get_row_layout() == "video_testimonial_block"){
				if( !in_array( "video-testimonials", $menu_array ) ){ 
					$menu_array[]="video-testimonials";
				}
			}elseif(get_row_layout() == "learn_more_block"){
				if( !in_array( "learn-more-block", $menu_array ) ){ 
					$menu_array[]="learn-more-block";
				}
			}	
		}; 
		if(!is_live_event())
			$menu_array[]="event-agenda";
		
		// $menu_array is available after here as a list of classes of all included blocks

		$menu_names = array();
		foreach($menu_array as $menu_name){
			$menu_name = str_replace('event-agenda', 'agenda', $menu_name);
			$menu_name = str_replace('learn-more-block', 'FAQ', $menu_name);
			$menu_name = str_replace('-block', '', $menu_name);
			$menu_name = str_replace('-', ' ', $menu_name);
			
			$menu_names[] = $menu_name;
		}
		
		echo '<div class="container"><div class="row">';
		
		echo '<div class="col-xs-12 col-md-9"><ul class="event-menu">';
		
		for($m=0;$m<count($menu_array);$m++){
			echo '<li><a href="#'.$menu_array[$m].'">'.$menu_names[$m].'</a></li>';
		}

		echo '</ul></div>';
		
		echo '<div class="col-xs-12 col-md-3"><a class="nmp-btn" href="#buy-tickets">Buy Tickets</a></div>';
		
		echo '</div></div>';
	}	
?>