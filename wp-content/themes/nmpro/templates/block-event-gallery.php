<?php
	if( get_sub_field('automatic_events') ){
		$number = get_sub_field('number_of_events');
		if( !$number ){
			$number = 1;
		}
		$eventObj = new doradoEvents;
		$eventObj->get_events('','','', $number );
		$events_by_stamp = $eventObj->events;
		ksort($events_by_stamp);
		foreach( $events_by_stamp as $stamp => $events_array ){			
			foreach( $events_array as $event ){
				$events[] = get_post( $event['post'] );
			}
		}				
	}
	else{
		$repeater = get_sub_field('events_repeater');
		foreach( $repeater as $row ) { 
			$events[] = $row['event'];
		}
	}			
?>
<?php if(is_array($events)) : ?>				
	<div class="row cblock events-block">
		<div class="container-xl gallery events-gallery">

			<div class="gallery-item event-item hover-static col-md-3">
				<div class="event-content">
					<h1>Current<br/>Upcoming<br /><span class="serif">Events</span></h1>
					<a class="nmp-btn white-btn" href="/events">See All</a>																		
				</div>
			</div>

			<?php foreach( $events as $event ) : ?>
				<?php if (is_object( $event )) : ?>
					<?php	$start_timestamp = get_field("start_timestamp",$event->ID);
							$month = date( 'F', $start_timestamp);
							$day = date( 'j', $start_timestamp);
							$event_excerpt = get_field("landing_excerpt",$event->ID);
					?>

					<div class="gallery-item event-item col-md-3"  style="background-image: url('<?php  echo wp_get_attachment_url( get_post_thumbnail_id($event->ID) ); ?>');">
						<div class="event-content">
							<h4 class="month"><?php echo $month; ?></h4>
							<div class="day"><?php echo $day; ?></div>
							<h3><?php echo $event->post_title; ?></h3>
							<?php echo $event_excerpt; ?>
							<a class="nmp-btn white-btn" href="<?php echo get_permalink($event->ID); ?>">View</a>																		
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
