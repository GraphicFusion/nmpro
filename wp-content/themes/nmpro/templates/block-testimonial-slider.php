<?php
	if( get_sub_field('automatic_testimonials') ){
		$number = get_sub_field('number_of_testimonials');
		if( !$number ){
			$number = 1;
		}
		$args = array('post_type' => 'testimonial','posts_per_page'=>$number);
		$result = new WP_Query($args);
		if($result->have_posts()){
			$testimonials = $result->posts;
		}				
	}
	else{
		$repeater = get_sub_field('testimonials_repeater');
		foreach( $repeater as $row ) { 
			$testimonials[] = $row['testimonial'];
		}
	}
?>
<div class="row testimonial-slider-block cblock">
	<div class="container">
		<?php if(is_array($testimonials)) :?>
			<ul class="testimonial-slider">
				<?php foreach( $testimonials as $testimonial ) : //print_r($testimonial); 
					$image_array = get_field('product_testimonial_image', $testimonial->ID );
					$tagline = get_field('product_testimonial_tagline', $testimonial->ID );
					$testimonial_quote = get_field('testimonial', $testimonial->ID );
				?>

					<li>
						<p><?php echo $testimonial_quote; ?></p>
						<div class="attr">
							<?php optimal_image( array( 'image' => $image_array )); ?>						
							<label name="name"><?php echo $testimonial->post_title; ?></label>
							<label name="title"><?php echo get_field('product_testimonial_tagline', $testimonial->ID ); ?></label>
							<div class="col-xs-12 hidden-md"><p><a class="next-testimonial">Next Testimonial</a></p></div>
						</div>
					</li>

				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>