<?php global $i;
if(!empty($i)){ $i = "";}

if(isset($_POST['login'])){

	require_once(get_template_directory()."/lib/infusionsoft/isdk.php");
	$app = new iSDK;
	$app->cfgCon('inf');

	$qry = array('Email'=> $_POST['username']);
	$rets= array('Id','Email');
	$contacts=$app->dsQuery("Contact",50,0,$qry,$rets);

	$noTagscontact = array();
	$Tags = array();
	foreach($contacts as $id){
		$query = array('ContactId' => $id['Id']);
		$returnFields = array('GroupId','ContactId');
		$groups = $app->dsQuery("ContactGroupAssign",1000,0,$query,$returnFields);

		if(empty($groups)){      // getting contacts id without tags 
			$noTagscontact[]=$id['Id'];
		}else{
			foreach($groups as $group){
				$Tags[]=$group['GroupId']; //getting tags id
			}
		}
	}
	foreach($noTagscontact as $contactid){ // adding tags to that contacts which are not assigned previously
		foreach($Tags as $tagID){
			$app->grpAssign((int)$contactid, (int)$tagID);
		//echo "((int)$contactid, (int)$tagID)";   
		}
	} 	
	
	/*if(in_array('773',$Tags) || in_array('771',$Tags)){ // 771- 2015 GPRM - US Stream Spanish, 773- 2015 GPRM US Stream
		$i = 1;
	}else{
		$msg = "Email not found. Please register to watch Events.";	
	}*/	


	 $valid_event_codes = explode(',', get_field('event_code'));
		foreach ($valid_event_codes as $event_code){
			if(in_array(trim($event_code),$Tags)){
				$i = 1;
				break;
			}
		}
		if($i!=1)
			$msg = "Email not found. Please register to watch Event.";
} 

if($i==1 ){
	// get list of all zones; 
	$allzones = timezone_identifiers_list();	
	// get this events zone array key
	$tz_key = get_field('timezone'); 
	// set zone
	$tz = $allzones[$tz_key];

	$zone = new DateTimeZone($tz);
	$noww = new DateTime("now", $zone);
	$tz_timestamp = get_field('start_timestamp') - $zone->getOffset($noww);

?>

	<div class="row live-event-block cblock no-gutter">
		<div class="container">
			<a class="anchor" name="live-event-video"></a>
			<h1><?php the_title(); ?></h1>
			<div class="row">
				<div class="live-video col-xs-12 col-lg-8 col-lg-offset-2">
					<div class="video">
						<?php echo get_field('streaming_code'); ?></div>
					</div>
				</div>
			    <div id="countdown-timer" class="col-xs-12 col-lg-8 col-lg-offset-2">
			    	<h3>Countdown to Live Event:</h3>
			    	<div id="clock"></div>
			    </div>
				 <script type="text/javascript">
					var eventTime = new Date((<?php echo $tz_timestamp; ?>)*1000);
					jQuery(document).ready(function(){
						jQuery('#clock').countdown(eventTime, function(event) {
							jQuery(this).html(event.strftime('%D:%H:%M:%S'));
							if (event.elapsed){
								jQuery('#countdown-timer').css('display','none');
								jQuery('.live-video').css('display','block');
							}else{
								jQuery('.live-video').css('display','none');	
							}
						});
					});
					</script>
			</div>
		</div>
	</div>
	
<?php }elseif($i!=1){?>

	<div class="row live-event-block cblock no-gutter">
		<div class="container">
			<a class="anchor" name="webinar-login"></a>
			<h1><?php the_title(); ?></h1>
			<div class="row">
				<form name="webinar_login"  method="post" action="" class="col-xs-12 col-lg-8 col-lg-offset-2">
					<?php if(isset($msg)){ ?><div class="webinar-login-msg"><?php echo $msg; ?></div><?php } ?>
					<legend>Enter your Email to watch Event:</legend>
					<input name="username" type="email" required="required" class="form-control" />
					<input type="submit" name="login" value="Submit" class="btn btn-lg btn-primary"  /></div>
				</form>
			</div>
		</div>
	</div>

	<?php //get_template_part( 'templates/block', 'buy-tickets' ); ?>

<?php } ?>