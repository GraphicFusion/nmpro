<?php global $v; // this template is for a video contained with 12 columns (as in a single video page or featured on archive ?>
<div class="featured-video col-md-12">
	<a class="video-thumb youtube-video" href="https://www.youtube.com/embed/<?php echo $v->youtube; ?>?rel=0&autohide=2&modestbranding=1&autoplay=1" style="background-image:url(<?php echo $v->background_image; ?>);"></a>

	<?php if( is_array( $v->categories) ): ?>
		<h5 class="video-cat">
			<?php foreach( $v->categories as $cat_array ) : ?>
				<a href="<?php echo $cat_array->slug; ?>"><?php echo $cat_array->name; ?></a>&nbsp;
			<?php endforeach; ?>
		</h5> 
	<?php endif; ?>

	<h2>
		<?php echo $v->post_title; ?>
	</h2>

</div>