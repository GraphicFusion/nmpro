<?php global $event; 
	$event_locations = get_field('event_locations', $event->ID );
	foreach( $event_locations as $key=>$location_array ){
		$location = $location_array['event_location'];
		$location_meta = get_field('loc_location', $location->ID );
		foreach($location_meta as $key=>$val){
			$location->$key = $val;
		}
	}
	if( is_object( $location ) ): 
		$location->custom_address = get_field('custom_address', $location->ID);	
		$location->loc_phone_number = get_field('loc_phone_number', $location->ID);	
?>
		
		<div class="row location-block cblock">
			<a class="anchor" name="location-block"></a>
					<div class="map-wrap">
						<div id='map-canvas' class="locations-map" style="height:600px;">
						</div>
					</div>
					<script>
						// setting up map.js variables here for ease of access; could output as php variables;
						markers = [[ '<?php echo $location->post_title; ?>','<?php echo $location->lat; ?>','<?php echo $location->lng; ?>', 'category']];
						var mapLat = <?php echo $location->lat; ?>;
						var mapLng = <?php echo $location->lng; ?>;
				
					</script>
				<div class="event-location">
					<h2><?php echo $event->location; ?></h2>
					<h4 class="loc-adr"><?php echo $location->custom_address; ?></h4>
					<h4 class="loc-phone">Phone Reservations: <?php echo $location->loc_phone_number; ?></h4>
					<h4>Group Code: <?php echo $event->group_code; ?></h4>
					<h3>Hotel Information</h3>
					<ul>
					<?php if( have_rows('pdfs', $location->ID) ): ?>
						<?php while ( have_rows('pdfs', $location->ID) ) : the_row(); 
								$file_array = get_sub_field('loc_file'); ?>  
							<li><a target="_blank" href="<?php echo $file_array['url']; ?>">Download <?php echo $file_array['title']; ?></a></li>
						<?php endwhile; ?>		
					<?php endif; ?>			
				</div>	
			</div>
		</div>
<?php endif; ?>

		
