<header class="navbar row" role="banner" id="themeslug-navbar">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="sidebar" data-target=".mobilemenu-one">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		
			<?php if (has_nav_menu('secmobile-menu')) : //-----| Second mobile menu (if on) ?>

				<button type="button" class="navbar-toggle toggle-two" data-toggle="sidebar" data-target=".mobilemenu-two">
					<span class="sr-only">Toggle Shop</span>
					<div class="icon-cart2"></div>				
				</button>

			<?php endif ?>

			<div class="navbar-brand">
				<a class="header-logo" href="<?php echo home_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt=""/ ">
				</a>
			</div>

			<nav class="navbar-collapse collapse utility-nav" role="navigation">

				<?php
					if (has_nav_menu('utility_navigation')) {
						wp_nav_menu(array('theme_location' => 'utility_navigation', 'menu_class' => 'nav navbar-nav'));
					};
					// see /lib/nav.php add_last_nav_item() for adding phone number (fields defined in acf-site-settings.php and set in admin site-settings-header
				?>
			</nav>
			<nav class="navbar-collapse collapse main-nav" role="navigation">

				<?php
					if (has_nav_menu('main_navigation')) {
						wp_nav_menu(array('theme_location' => 'main_navigation', 'menu_class' => 'nav navbar-nav'));
					};
				?>

			</nav>
		</div>
	</div>
</header>
<div class="row action-bar">
<?php get_template_part( 'templates/block', 'header-action-block' ); ?>
</div>