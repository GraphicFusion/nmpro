<?php if( $key_speakers = get_sub_field('key_speakers',$post->ID) ):  
	$bg_image_html = "";
	if( $bg_image = get_sub_field('key_speakers_image',$post->ID) ){
		$bg_image_html = 'style="background-image:url('.$bg_image['url'].')"';
	}
?>
<div class="row key-speakers cblock" <?php echo $bg_image_html; ?>>
		<a class="anchor" name="key-speakers"></a>
		<div class="container">
			<h1>Key Speakers</h1>
			<?php foreach( $key_speakers as $key_speaker ) : ?>
				<?php
					$key_speaker_name = $key_speaker->post_title;
					$key_speaker_tag = get_field('speaker_tagline', $key_speaker->ID);
					$key_speaker_bio = get_field('speaker_bio', $key_speaker->ID);
					$key_speaker_image = get_field('speaker_image', $key_speaker->ID); 
				?>
				<div class="speaker col-md-12 col-lg-8 col-lg-offset-2">
					<div class="col-md-4">
						<?php if( is_array( $key_speaker_image ) ){
								$image = $key_speaker_image['url'];
								$args = array ( 'image' => $image, 'lazy'=>'load' , 'width'=>'150', 'height'=>'150');
								// https://bitbucket.org/tripgrass/optimal-image
								optimal_image( $args );
							}				
							else{
								$default_image = get_template_directory_uri().'/assets/img/default_profile.jpg';
								$args = array ( 'image' => $default_image, 'lazy'=>'load' , 'width'=>'150', 'height'=>'150');
								// https://bitbucket.org/tripgrass/optimal-image
								optimal_image( $args );
							}
						?>
					</div>
					<div class="col-md-8">
						<h2><?php echo $key_speaker_name; ?></h2>		
						<h3><?php echo $key_speaker_tag; ?></h3>		
						<p><?php echo $key_speaker_bio; ?></p>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
