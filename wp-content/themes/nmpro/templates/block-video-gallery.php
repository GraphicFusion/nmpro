<?php
	// check if custom field calls for number of videos, OR if is called directly (like single-video.php)
	if( get_sub_field('automatic_videos') || ( !get_sub_field('automatic_videos') && !get_sub_field('videos_repeater') ) ){
		$number = 0;
		$exclude_posts = array();
		if( get_sub_field('automatic_videos') ){
			$number = get_sub_field('number_of_videos');
		}
		else{
			$exclude_posts[] = $post->ID;
		}
		if( !$number || $number > 4 ){
			$number = 4;
		}
		$args = array(
			'post_type' => 'video',
			'posts_per_page'=>$number,
			'post__not_in' => $exclude_posts
		);
		if( 1==1 ){
			$term = "testimonial";
			$args['tax_query'] = array(
				'taxonomy' => 'video_cat',
				'field' => 'slug',
				'terms' => $term,
			);
		}
		$result = new WP_Query($args);
		if($result->have_posts()){
			$videos = $result->posts;
		}				
	}
	else{
		$repeater = get_sub_field('videos_repeater');
		$i = 0;
		foreach( $repeater as $row ) {
		 	if( $i < 4 ){
				$vid = get_post( $row['video'] );
				$vid->cat_slug = $row['video_category']->slug;
				$vid->category = $row['video_category']->name; 
				$videos[] = $vid;
				$i++;
			}
		}
	}
?>

<div class="row cblock video-gallery-block">
	<div class="container gallery">
	
		<header class="row">
			<div class="col-xs-12">
				<h3>Latest Videos</h3>
				<a href="/videos" class="nmp-btn">See All</a>
			</div>			
		</header>
		<?php if(is_array($videos)) : ?>
			<?php foreach( $videos as $video ) : 
				$v = get_video_meta( $video ); // see lib/custom-videos.php
				$category = "";
				$cat_slug = "";
				if( property_exists( $v , 'category' ) ) {
					$category = $v->category;
					$cat_slug = $v->cat_slug;
				}
				elseif( is_array( $v->categories ) and count($v->categories)>0 ){
					$key = array_rand( $v->categories);
					$cat_array = $v->categories[$key];
					$category = $cat_array->name;
					$cat_slug = $cat_array->slug;
				}
			?><div class="gallery-item col-xs-12 col-sm-6 col-md-3">
					<a class="video-thumb youtube-video" href="https://www.youtube.com/embed/<?php echo $v->youtube; ?>?rel=0&autohide=2&modestbranding=1&autoplay=1" style="background-image:url(<?php echo $v->background_image; ?>);">
						<?php if( $v->durationText ) : ?>
							<div class="time"><?php echo $v->durationText; ?></div>
						<?php endif; ?>
					</a>
				
					<!--// temporary hide for category <h5 class="video-cat"><a href="/video-cat/<?php echo $cat_slug; ?>"><?php echo $category; ?></a></h5>-->
					<p>
						<?php
							$str = substr( $v->excerpt, 0, 80);
							$result = preg_replace( "/\s[a-z:]+$/i", "", $str );
							if( $str ) : echo $str; ?> <a href=''>...</a>
						<?php endif; ?>
					</p>
				</div><?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
