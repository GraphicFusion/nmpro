<?php if( $guests = get_sub_field('guest_speakers',$post->ID) ):  ?>
	<div class="row guest-speakers cblock">
		<a class="anchor" name="guest-speakers"></a>
		<div class="container">
			<h1>Guest Speakers</h1>
			<div class="speaker-boxes">
				<?php 
					$guest_bios = array();
					$a=0;
					foreach( $guests as $guest ) :
						$guest_name = $guest->post_title;
						$guest_tag = get_field('speaker_tagline', $guest->ID);
						$guest_bio = get_field('speaker_bio', $guest->ID);
						if($guest_bio && $guest_bio!='')					
							$guest_bios['bio'.$a] = $guest_bio;
						
						$guest_image = get_field('speaker_image', $guest->ID); 
					?><div class="speaker col-xs-12 col-sm-6 col-md-4 col-lg-3 <?php if($guest_bio && $guest_bio!='' && count($guest_bio)==1) echo 'active'; ?>">
						<?php if( is_array( $guest_image ) ){
								$image = $guest_image['url'];
								$args = array ( 'image' => $image, 'lazy'=>'load' , 'width'=>'300', 'height'=>'300');
								// https://bitbucket.org/tripgrass/optimal-image
								optimal_image( $args );
							}				
							else{
								$default_image = get_template_directory_uri().'/assets/img/default_profile.jpg';
								$args = array ( 'image' => $default_image, 'lazy'=>'load' , 'width'=>'300', 'height'=>'300');
								// https://bitbucket.org/tripgrass/optimal-image
								optimal_image( $args );
							}
						?>
						<h6 <?php if(!$guest_bio || $guest_bio=='') echo 'class="none"'; ?>><?php echo $guest_name; ?></h6>		
					</div><?php $a++; endforeach; ?>
			</div>
			<?php if(count($guest_bios)>0): ?>
				<div class="guest-bio-box">
					<?php $a_bio = 0;
					foreach( $guest_bios as $guest_bio => $bio): ?>
					<div class="guest-bio <?php if($a_bio==0) echo 'active'; ?>" id="<?php echo $guest_bio; ?>">
						<?php echo $bio; ?>
					</div>
					<?php $a_bio++;
					endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>