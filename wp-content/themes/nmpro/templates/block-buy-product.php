<?php if( 1==1 ):?>
	<div class="row buy-product cblock">
		<div class="container">
			<div class="col-md-12 col-lg-8 col-lg-offset-2">
				<?php if( have_rows('price_packages_en') ): ?>
					<?php while ( have_rows('price_packages_en') ) : the_row(); ?>
						<div class="col-md-6">			
							<h3><?php echo the_sub_field('price_text_en'); ?></h3>
							<a href="<?php echo the_sub_field('url_en'); ?>" class="nmp-btn">Buy Now</a>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>