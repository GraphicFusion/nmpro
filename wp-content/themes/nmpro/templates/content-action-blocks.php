<div class='container'>

<?php while(have_rows("action_block",'option')): the_row();?>

	<?php if(get_row_layout() == "action_email"): ?>

		<form accept-charset="UTF-8" action="https://tw126.infusionsoft.com/app/form/process/512ff01005ebce699be2bdaae23a6595" class="infusion-form" method="POST"|form accept-charset="UTF-8" action="https://tw126.infusionsoft.com/app/form/process/512ff01005ebce699be2bdaae23a6595" class="infusion-form" method="POST">
			<span class='float'><?php echo get_sub_field('email_call_to_action'); ?></span>
			<input name="inf_form_xid" type="hidden" value="512ff01005ebce699be2bdaae23a6595" />
			<input name="inf_form_name" type="hidden" value="Sign up for newsletter - Sidebar Optin" />
			<input name="infusionsoft_version" type="hidden" value="1.46.0.37" />
			<input id="inf_field_Email" class="infusion-field-input-container" type="text" name="inf_field_Email">
			<input type='submit' value='Send' />
		</form>
		<script type="text/javascript" src="https://tw126.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=6b57c6f732c4327e987687788cc807a0"|script type="text/javascript" src="https://tw126.infusionsoft.com/app/webTracking/getTrackingCode?trackingId=6b57c6f732c4327e987687788cc807a0"></script>

	<?php elseif(get_row_layout() == "action_button"): ?>

		<?php echo get_sub_field('action_button_call_to_action'); ?>
		<button><?php echo get_sub_field('action_button_text'); ?></button>

	<?php endif; ?>

<?php endwhile; ?>

</div>