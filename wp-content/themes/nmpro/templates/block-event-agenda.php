<?php global $event; 
	$daystamp = $event->start_timestamp;
	if( count( $event->agenda ) > 0 ) : ?>
	<div class="row event-agenda cblock">
		<a class="anchor" name="event-agenda"></a>
		<div class="container">
			<h1>Event Agenda</h1>
			<?php if(get_field('agenda_subtitle')) echo '<h2>'.get_field('agenda_subtitle').'</h2>'; ?>
			<ul class="nav nav-pills">
				<?php $i = 1; foreach( $event->agenda as $day ) : ?>
					<li role="presentation" class="day-<?php echo $i; if( $i == 1 ){ echo ' active'; } ?>" ><a href="#">Day <?php echo $i; ?><span class="date"><?php echo date( 'M d', $daystamp); ?></span></a></li>
				<?php $i++; $daystamp = $daystamp + (60*60*24); endforeach; ?>
			</ul>

			<div class="agenda-box col-md-12 col-lg-10 col-lg-offset-1">
				<?php $i=1; foreach( $event->agenda as $day ) : ?>
					<div class="agenda-day day-<?php echo $i; if( $i == 1 ) echo ' active';?>">
						<?php foreach( $day as $agenda_time => $item ) : ?>		
							<div class="agenda-item">
								<div class="agenda-time col-md-3"><?php echo date( 'g:i A' , $agenda_time); ?></div>
								<div class="agenda-name col-md-9"><?php echo $item['name']; ?></div>
								<?php if ( is_array( $item['speakers'] ) && 1==2) : ?>
									<div class="agenda-speakers">
										<ul>
											<?php foreach($item['speakers'] as $arr) : $speaker = $arr['agenda_speaker']; ?>
												<li><?php echo $speaker->post_title; ?></li>
											<?php endforeach; ?>
										</ul>
									</div>
								<?php endif; ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php $i++; endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>