<?php $event = get_event( $post ); // defined in custom-events.php ?>

<?php if( is_live_event() ) get_template_part( 'templates/block', 'live-event' ); ?>

<?php while(have_rows("event_landing_content")): the_row(); ?>

	<?php if(get_row_layout() == "events_video_block"): ?>

		<?php if( !is_live_event() ) : ?>
	
			<?php get_template_part( 'templates/block', 'event-video' ); ?>
	
		<?php endif; ?>

	<?php elseif(get_row_layout() == "read_more_block"): ?>

		<?php get_template_part( 'templates/block', 'read-more' ); ?>

	<?php elseif(get_row_layout() == "key_speakers_block"): ?>

		<?php get_template_part( 'templates/block', 'key-speakers' ); ?>

	<?php elseif(get_row_layout() == "guest_speakers_block"): ?>

		<?php get_template_part( 'templates/block', 'guest-speakers' ); ?>

	<?php elseif(get_row_layout() == "video_testimonial_block"): ?>

		<?php get_template_part( 'templates/block', 'video-testimonial' ); ?>

	<?php elseif(get_row_layout() == "learn_more_block"): ?>

		<?php get_template_part( 'templates/block', 'learn-more' ); ?>

	<?php endif; ?>
	
<?php endwhile; ?>
<?php if( !is_live_event() ) : ?>
	<?php get_template_part( 'templates/block', 'event-agenda' ); ?>
	<?php get_template_part( 'templates/block', 'event-location' ); ?>
<?php endif; ?>
<?php get_template_part( 'templates/block', 'buy-tickets' ); ?>
