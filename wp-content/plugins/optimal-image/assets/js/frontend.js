	// inline script output from optimal-image.php; only printed when a page has an optimal-image img element
	var oi_lazy = [],
		oi_lazyInView = [];
	bp_type = 'desktop';

	if( window.innerWidth <= $breakpoint ){
		bp_type = 'mobile';
	}
	// looks for class of oi_parent on image's parent; returns parent if no class found;
	function checkParent( el , i ){
		var thisParent = el.parentNode;
		if(i < 3){
			if ( (' ' + thisParent.className + ' ').replace(/[\n\t]/g, ' ').indexOf(' oi_parent ') > -1 ){
				thisParent.className = thisParent.className + ' loaded';
				return true;
			}
			else{
				i++;
				checkParent( thisParent, i ); 
			}
		}
	}
	window.set_src = function ( selector , after_load ){
		var el = document.querySelector( '#' + selector ), $i = 0, JSONattributes, lazyLoad, background, lazyInview, newUrl;
		if( el ){
			// add loaded class on image after it has loaded
			if (el.addEventListener) {
				el.addEventListener('load', function() { 
					el.className = el.className + ' loaded';
					checkParent(el, 0);
				});
			}
			else {
				el.attachEvent('load', function() { 
					el.className = el.className + ' loaded';
					checkParent(el, 0);
				});
			}
			JSONattributes = el.getAttribute('data-image-sizes');
				lazyLoad = el.getAttribute('data-oi-lazy-load');
				background = el.getAttribute('data-oi-background');
				lazyInview = el.getAttribute('data-oi-lazy-inview');
		}
		if( 'undefined' != typeof JSONattributes ){
			var attributes = JSON.parse( JSONattributes ),
				image_type = attributes[bp_type],
				thisImage = attributes[image_type];
				if( typeof thisImage != 'undefined'){

					el.width = thisImage.width;
					el.height = thisImage.height;
					newUrl = thisImage.src;
				}
				else{
					newUrl = attributes.original.src;
				}
				if( !lazyLoad && !lazyInview || after_load){
					if( background ){
						el.style.backgroundImage = "url('" + newUrl + "')";
					}
					else{
						el.src = newUrl;
					}
				}
		}
	}