<?php
/*
 * Plugin Name: Optimal Image
 * Version: 0.0.1
 * Plugin URI: 
 * Description: Get blazing fast page speeds by lazy loading your images and serving device-specific image sizes  
 * Author: Trip Grass
 * Author URI: 
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: optimal-image
 *
 * @package WordPress
 * @author Trip Grass
 * @since 0.0.1
 */

if ( ! defined( 'ABSPATH' ) ) exit;

require_once( 'includes/class-optimal-image.php' );

/*
 * Creates the Optimal_Image class as a public function 
 *
 * Call the function in your template with <?php optimal_image( $args ); ?>
 * $args can be: 
 * 		(1)	a string of an image src like 'http:///www.something.com/wp-content/themes/thistheme/assets/img/this.jpg'
 *		(2) an acf image array 
 *		(3) a post object from which the featured image will be used
 * 
 * @author  Trip Grass
 * @since	1.0
 * @param	mixed	$image		(optional) STRING of image src path OR ARRAY of ACF image array;
 * @param	mixed	$post		(optional) Post OBJECT, optimal_image will use the featured image;
 * @param	string	$mobile		(optional) The image size identifier for images to be loaded for mobile (screen widths less than the breakpoint);
 *											Accepts wordpress defaults or custom size as added in add_image_size
 * @param	string	$mobile		(optional) The image size identifier for images to be loaded for desktop (screen widths greater than or equal to the breakpoint); 
 *											Accepts wordpress defaults or custom size as added in add_image_size
 * @param	string	$lazy		(optional) String ('load' or 'inView'), when set will lazy load on doc ready ('load');
 *											Or if WayPoints is included, will load when in view ('inView'); if 'inView' is called and Waypoints is not included, will load on doc ready;
 * @param	string	$classes	(optional) String of space separated class names to be added to the image element ('classOne classTwo');
 * @param	string	$title		(optional) String of image title - where not defined in the media post OR if defined, will override
 * @param	string	$description (optional) String of image description - where not defined in the media post OR if defined, will override
 * @param	string	$caption	(optional) String of image caption - where not defined in the media post OR if defined, will override
 * @param	string	$alt		(optional) String of image alt text - where not defined in the media post OR if defined, will override
 * @param	string	$width		(optional) String of image width - where not defined in the media post OR if defined, will override
 * @param	string	$height		(optional) String of image height - where not defined in the media post OR if defined, will override
 * @param	string	$background	(optional) If true, will output the included image element as a background image within a div element and will lazy load; 
 *											No extra classes loaded must be called like so: 
 *											$args = array( 'image' => $img, 'background' => true); ?> <div class="reveal fade-block" <?php optimal_image( $args ); ?> >
 */
function optimal_image( $args ){
	$optimal_image = optimal_image_main();
	$optimal_image->build_optimal_image( $args );
}

/**
 * Returns the main instance of optimal_image to prevent the need to use globals.
 *
 * @since  0.0.1
 * @return object optimal_image
 */
function optimal_image_main () {
	$bp = apply_filters( 'optimal_image/breakpoint', 480 );
	$base = substr( 'optimal_image' , 0 , 4 )."_";
	$options = array (
		'environment' => 'development',
		'base' => $base,
		'breakpoint' => $bp
	);
	$instance = optimal_image::instance( __FILE__, '0.0.1' , $options );
	return $instance;
}

