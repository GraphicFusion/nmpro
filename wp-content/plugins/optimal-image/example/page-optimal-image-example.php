<?php get_header(); // Template Name: Optimal Image Test
	/* (1) Add this file to your theme folder (/wp-content/themes/your-theme) 
	 * (2) Add an ACF Gallery where location is Page Template = Optimal Image Test. Name the Field 'optimal_image_acf_gallery'. If using a single image, output must be 'array'. 
	 * (3) Add a new page using this template
	 * (4) Add a featured image to that page
	 * (5) Add images to the ACF Gallery field.
	 * (6) The hardcoded test image (any image, but one is included in the optimal-image/example folder) should be saved to the /wp-content/themes/your-theme/assets/img OR repathed
	 * (7) Lazy Loading requires jQuery
	 */

?>
<div id="content">

	<div class="wrap">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article <?php post_class(); ?>>

			<header class="article-header">
				<h1 class="page-title"><strong>PAGE TITLE:</strong> <?php the_title(); ?></h1>
			</header>
			<p>
				In your functions.php file, add the following to set the breakpoint in pixels (the default and this example, use 480):
				<pre style="background-color:#e6e6e6; padding:10px; display:inline-block;">add_filter( 'optimal_image/breakpoint', function(){ return 1170; } );</pre>
			</p>
			<section class="entry-content">
				<hr>

				<h2>Hard Coded mobile.jpg and desktop.jpg</h2>
				<p>Two hardcoded options are available for this method. Size your screen with a width greater than the breakpoint and the word "DESKTOP" should appear. Smaller than the breakpoint, and "MOBILE" should appear.</p> 
				<div class="oi_parent">
					<?php 
						$args = array (
							'image'	=> get_template_directory_uri() . '/assets/img/mobile.jpg',
							'width' => 173,
							'height' => 35,
							'desktop_image'	=> get_template_directory_uri() . '/assets/img/desktop.jpg',
							'desktop_width' => 208,
							'desktop_height' => 35,
							'lazy'	=> 'load'
						);
						optimal_image( $args );
					?>
				</div>

				<hr>

				<h2>Featured Image</h2>
				<p>This pulls in a post's featured image. It takes the $post OBJECT. This is lazy loaded.</p>
				<?php 
					$args = array (
						'post'	=> $post,
						'lazy'	=> 'load'
					); 
					optimal_image( $args );
				?>
				
				<hr>

				<h2>ACF Gallery</h2>
				<p>Requires an ACF gallery named 'optimal_image_acf_gallery'.</p>
				<?php $images = get_field('optimal_image_acf_gallery'); if( $images ): ?>

					<ul>
					    <?php foreach( $images as $image ): ?>
						    <?php 
								$args = array (
									'image'		=> $image,
									'mobile'	=> 'thumbnail',
									'desktop'	=> 'large',
									'lazy'		=> 'load'
								); 
							?>
							<li>
								<a href="<?php echo $image['url']; ?>">
									<?php optimal_image( $args ); ?>
								</a>
								<p><?php echo $image['caption']; ?></p>
							</li>
					    <?php endforeach; ?>
					</ul>
				<?php endif; ?>

			</section>

		</article>

		<?php endwhile; endif; ?>

	</div>

</div>

<?php get_footer(); ?>